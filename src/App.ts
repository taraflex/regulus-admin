import { Singleton } from 'typescript-ioc';

@Singleton
export class App {
    constructor() { }
    async init() { }
}