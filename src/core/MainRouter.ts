import Router from '@koa/router';
import { Singleton } from 'typescript-ioc';

import catchError from '@middlewares/catch-error';
import { HEALTH } from '@utils/config';

@Singleton
export class MainRouter extends Router<any, any> {
    constructor() {
        super();
        this
            .use(catchError)
            .get('health', HEALTH, ctx => {
                ctx.noStore();
                ctx.type = 'json';
                ctx.body = '{"success":"ok"}';
            })
            .get('index', '/', ctx => {
                ctx.status = 200;
                ctx.noStore();
                ctx.body = '';
            });
    }
}