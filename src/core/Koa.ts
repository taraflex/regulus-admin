import etag from 'etag';
import koa, { Context } from 'koa';
import plur from 'plur';
import { compileTemplate, LocalsObject } from 'pug';
import { Inject, Singleton } from 'typescript-ioc';

import { Settings } from '@entities/Settings';
import Router from '@koa/router';
import { IS_LOCAL, PORT, STATIC } from '@utils/config';

import type { SetOption } from 'cookies';

export const DEFAULT_COOKIE_OPTIONS = Object.freeze({
    maxAge: 60 * 3 * 1000,
    secure: false,
    httpOnly: true,
    sameSite: 'lax',
    signed: false,
    overwrite: true
}) as SetOption;

const DELETE_COOKIE_OPTIONS: SetOption = Object.freeze({
    ...DEFAULT_COOKIE_OPTIONS,
    maxAge: undefined,
    expires: new Date(-1),
    signed: false
});

declare module 'koa' {
    interface Context {
        sendNoneMatch(body: Buffer | string): void;
        noStore(): void;

        router: Router;
        namedRedirect: (routName: string, delay?: number, params?: Record<string, any>) => void;
        resolve: (routName: string, params?: Record<string, any>) => string;

        pug: (file: string, locals?: LocalsObject) => void;

        isAjax: boolean;

        setCookie(name: string, data: any): void;
        getCookie<T = any>(name: string): T;
        deleteCookie(name: string): void;
    }
}


@Singleton
export class Koa extends koa {
    constructor(@Inject settings: Settings) {
        super();
        this.proxy = true;
        this.keys = [settings.secret];

        Object.defineProperty(this.context, 'isAjax', {
            get(this: Context) {
                return this.request.headers['x-requested-with'] === 'XMLHttpRequest';
            }
        });

        this.context.noStore = function (this: Context) {
            this.set('Cache-Control', 'no-store, no-cache, max-age=0');
        }

        this.context.sendNoneMatch = function (this: Context, body: Buffer | string) {
            const et = body && etag(body);
            if (et) {
                this.set('Cache-Control', 'no-cache, max-age=31536000');
                this.set('ETag', et);
                if (this.fresh) {
                    this.status = 304;
                    return;
                }
            }
            this.noStore();
            this.body = body;
        }

        this.context.resolve = function (this: Context, route: string, params?: Record<string, any>) {
            return this.origin + this.router.url(route, params ? Object.assign({}, this.params, params) : this.params);
        };

        this.context.namedRedirect = function (this: Context, route: string, delay?: number, params?: Record<string, any>) {
            this.noStore();
            const target = this.resolve(route || 'root', params);
            if (delay > 0) {
                this.type = 'html';
                //в chrome баг, который не дает установить куку при редиректе через location или refresh приходится извращаться
                this.body = `<html><head><meta http-equiv="refresh" content="${delay};url=${target}" /></head><body>Redirect after ${delay} ${plur('second', delay)}</body></html>`;
            } else {
                this.redirect(target);
            }
        };

        this.context.pug = function (this: Context, file: string, locals?: LocalsObject) {

            this.type = 'html';
            const template: compileTemplate = require('./templates/' + file + '.pug');

            const assets: Record<string, { css?: string, js?: string }>[] = (locals?.entries || ['common']).map(e => __non_webpack_require__(__dirname + '/' + e + '-assets.json'));

            const styles = assets.flatMap(r => Object.values(r).map(entry => entry.css));
            const scripts = assets.flatMap(r => Object.values(r).map(entry => entry.js));

            this.sendNoneMatch(template({
                baseUrl: this.protocol + '://' + (IS_LOCAL && !this.secure ? '127.0.0.1:' + PORT : this.host) + STATIC,
                title: APP_TITLE,
                ...this.flash,
                ...locals,
                styles,
                scripts
            }));
        };

        this.context.setCookie = function (this: Context, name: string, data: any) {
            this.cookies.set(name, encodeURIComponent(JSON.stringify(data)), DEFAULT_COOKIE_OPTIONS);
        }

        this.context.deleteCookie = function (this: Context, name: string) {
            this.cookies.set(name, '', DELETE_COOKIE_OPTIONS);
            this.cookies.set(name + '.sig', '', DELETE_COOKIE_OPTIONS);//удалим также подпись
        }

        this.context.getCookie = function (this: Context, name: string) {
            try {
                const v = this.cookies.get(name);
                return v ? JSON.parse(decodeURIComponent(v)) : null;
            } catch (err) {
                LOG_ERROR(name, err);
                this.deleteCookie(name);
            }
            return null;
        }
    }
}
