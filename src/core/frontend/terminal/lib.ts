export { Terminal } from 'xterm';
export { WebLinksAddon } from 'xterm-addon-web-links';
export { FitAddon } from 'xterm-addon-fit';