import './index.css';

import Vue from 'vue';
import Component from 'vue-class-component';

import notify from '@utils/notify';

import type { Terminal, WebLinksAddon, FitAddon } from './lib';
import importEntry from '@utils/import-entry';

@Component
export default class terminal extends Vue.extend({
    props: {
        resizeEmmiter: Set
    }
}) {
    static readonly icon = 'terminal';

    $refs!: {
        terminal: HTMLElement
    }

    protected loading = true;

    protected webLinksAddon?: WebLinksAddon;
    protected terminal?: Terminal;
    protected fitAddon?: FitAddon;
    protected onResize?: () => void;

    async mounted() {
        try {
            const { Terminal, WebLinksAddon, FitAddon } = await importEntry<typeof import('./lib')>('xterm');

            const t = this.terminal = new Terminal({
                fontSize: 13,
                fontFamily: "Consolas, 'Courier New', monaco, monospace",
                cols: 1,
                rows: 1,
                cursorBlink: true,
                cursorStyle: 'bar',
            });

            t.loadAddon(this.webLinksAddon = new WebLinksAddon());
            t.loadAddon(this.fitAddon = new FitAddon());
            t.open(this.$refs.terminal);
            this.fitAddon.fit();

            this.resizeEmmiter.add(this.onResize = () => this.fitAddon.fit());

            await this.$rpc.enableTerminal(t);
        } catch (err) {
            this.resizeEmmiter.delete(this.onResize);
            this.webLinksAddon?.dispose();
            this.fitAddon?.dispose();
            this.terminal?.dispose();
            notify(err);
        } finally {
            this.loading = false;
        }
    }

    beforeDestroy() {
        this.resizeEmmiter.delete(this.onResize);
        this.webLinksAddon?.dispose();
        this.fitAddon?.dispose();
        this.terminal?.dispose();
    }

    render() {
        return <div v-loading={this.loading} class="rg-data_single">
            <p class="rg-field__error">ВНИМАНИЕ! Зона повышенной ответственности. Тут можно уронить сервер окончательно и бесповоротно.</p>
            <div class="terminal-container" ref="terminal" />
        </div>
    }
}