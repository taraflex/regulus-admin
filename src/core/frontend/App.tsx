import Vue from 'vue';
import Component from 'vue-class-component';
import VueRouter from 'vue-router';

import { httpJson } from '@utils/axios-smart';
import delay from '@utils/delay';
import notify from '@utils/notify';
import readBlob from '@utils/read-blob';

@Component
export default class extends Vue {
    $router!: VueRouter;
    loading = false;
    onError(errors) {
        notify(errors?.errors || errors);
        this.$rpc.restart();
    }
    run<Args>(fn: string, ...args: Args[]) {
        fn && this[fn](...args);
    }
    async logout() {
        try {
            this.loading = true;
            this.$rpc.dispose();
            await httpJson.put(Paths.logout);
            window.location.href = Paths.login;
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }
    changePassword() {
        window.location.href = Paths.resetpass;
    }
    changeEmail() {
        window.location.href = Paths.resetemail;
    }
    async waitServerRestart(): Promise<never> {
        for (; ;) {
            try {
                await delay(2000);
                if ((await httpJson.get(Paths.health)).data.success === 'ok') {
                    window.location.reload();
                }
            } catch { }
        }
    }
    async restartServer() {
        try {
            this.loading = true;
            this.$rpc.dispose();
            await httpJson.put(Paths.restart).catch(e => e);
            await this.waitServerRestart();
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }
    async restore({ target }) {
        try {
            const file: File = target.files[0];
            if (!file) {
                return;
            }
            if (!file.name.toLowerCase().endsWith('.sqlite3')) {
                throw 'Invalid database file extension.';
            }
            if (file.size <= 4096) {
                throw 'Database file size less than possible.';
            }
            this.loading = true;
            const data = await readBlob(file);
            this.$rpc.dispose();
            await httpJson.put(Paths.restore, data);
            await this.waitServerRestart();
        } catch (err) {
            this.onError(err);
        } finally {
            target.value = '';
            this.loading = false;
        }
    }
    render() {
        return <div class="rg-row" v-loading={this.loading} style="min-height:100%">
            <div class="rg-menu">
                <el-dropdown trigger="click" placement="bottom-start" vOn:command={this.run}>
                    <div class="rg-menu__item btn">
                        <i class="rg-menu__icon fa fa-user-alt"></i><span>{UserInfo.email}</span>
                    </div>
                    <el-dropdown-menu slot="dropdown" class="rg-user-info-dropdown">
                        <el-dropdown-item command="changeEmail">Change email</el-dropdown-item>
                        <el-dropdown-item command="changePassword">Change password</el-dropdown-item>
                        <el-dropdown-item command="logout">Logout</el-dropdown-item>
                    </el-dropdown-menu>
                </el-dropdown>{
                    this.$router.options.routes.slice(0, -1).map(route => <router-link to={route.path} class="rg-menu__item btn">
                        {route.meta.icon && <i class={'rg-menu__icon fa' + (route.meta.icon.startsWith('_') ? 'b fa-' + route.meta.icon.replace(/^_/, '') : ' fa-' + route.meta.icon)}></i>}<span>{route.name}</span>
                    </router-link>)
                }
                <a download href={Paths.backup} target="_blank" class="rg-menu__item btn"><i class="rg-menu__icon fa fa-cloud-download-alt"></i><span>Backup</span></a>
                <label class="rg-menu__item btn"><input type="file" class="rg-menu__file" accept=".sqlite3" vOn:change={this.restore} /><i class="rg-menu__icon fa fa-cloud-upload-alt"></i><span>Upload backup</span></label>
                <div class="rg-menu__item btn" vOn:click={this.restartServer} ><i class="rg-menu__icon fa fa-power-off"></i><span>Restart server</span></div>
            </div>
            <keep-alive>
                <router-view></router-view>
            </keep-alive>
        </div>
    }
}