import Vue from 'vue';

import { BROWSER_ID, TAB_ID } from '@utils/axios-smart';
import notify from '@utils/notify';

//todo rewrite
export default class extends Vue.extend({
    props: {
        action: String,
        accept: String,
        title: String,
        limit: Number,
        value: Array,
        disabled: Boolean
    }
}) {
    onRemove(_, fileList) {
        this.$emit('input', fileList);
    }
    onSuccess(resp, file, fileList) {
        file.url = resp;
        this.$emit('input', fileList);
    }
    onError(err, _, fileList) {
        notify(err);
        this.$emit('input', fileList);
    }
    render() {
        return <el-upload action={this.action} with-credentials accept={this.accept} multiple limit={this.limit} file-list={this.value} disabled={this.disabled} vOn:success={this.onSuccess} vOn:error={this.onError} vOn:remove={this.onRemove} headers={{
            'x-requested-with': 'XMLHttpRequest',
            'x-tab': TAB_ID,
            'x-browser': BROWSER_ID
        }}><el-button type="primary" disabled={this.disabled}>{this.title || 'Click to upload'}</el-button></el-upload>
    }
}