import 'monaco-editor/esm/vs/language/typescript/monaco.contribution';
import 'monaco-editor/esm/vs/language/typescript/tsMode';
import 'monaco-editor/esm/vs/basic-languages/typescript/typescript.contribution';
import 'monaco-editor/esm/vs/basic-languages/typescript/typescript';
import 'monaco-editor/esm/vs/basic-languages/javascript/javascript.contribution';
import 'monaco-editor/esm/vs/basic-languages/javascript/javascript';
import 'monaco-editor/esm/vs/editor/browser/controller/coreCommands';
import 'monaco-editor/esm/vs/editor/browser/widget/codeEditorWidget';
import 'monaco-editor/esm/vs/editor/contrib/bracketMatching/bracketMatching';
import 'monaco-editor/esm/vs/editor/contrib/caretOperations/caretOperations';
import 'monaco-editor/esm/vs/editor/contrib/caretOperations/transpose';
import 'monaco-editor/esm/vs/editor/contrib/clipboard/clipboard';
import 'monaco-editor/esm/vs/editor/contrib/codeAction/codeActionContributions';
import 'monaco-editor/esm/vs/editor/contrib/codelens/codelensController';
import 'monaco-editor/esm/vs/editor/contrib/comment/comment';
import 'monaco-editor/esm/vs/editor/contrib/contextmenu/contextmenu';
import 'monaco-editor/esm/vs/editor/contrib/cursorUndo/cursorUndo';
import 'monaco-editor/esm/vs/editor/contrib/dnd/dnd';
import 'monaco-editor/esm/vs/editor/contrib/find/findController';
import 'monaco-editor/esm/vs/editor/contrib/folding/folding';
import 'monaco-editor/esm/vs/editor/contrib/format/formatActions';
//import 'monaco-editor/esm/vs/editor/contrib/goToDefinition/goToDefinitionCommands';
//import 'monaco-editor/esm/vs/editor/contrib/goToDefinition/goToDefinitionMouse';
import 'monaco-editor/esm/vs/editor/contrib/gotoError/gotoError';
import 'monaco-editor/esm/vs/editor/contrib/hover/hover';
import 'monaco-editor/esm/vs/editor/contrib/inPlaceReplace/inPlaceReplace';
import 'monaco-editor/esm/vs/editor/contrib/linesOperations/linesOperations';
import 'monaco-editor/esm/vs/editor/contrib/links/links';
import 'monaco-editor/esm/vs/editor/contrib/multicursor/multicursor';
import 'monaco-editor/esm/vs/editor/contrib/parameterHints/parameterHints';
//import 'monaco-editor/esm/vs/editor/contrib/referenceSearch/referenceSearch';
import 'monaco-editor/esm/vs/editor/contrib/rename/rename';
import 'monaco-editor/esm/vs/editor/contrib/smartSelect/smartSelect';
import 'monaco-editor/esm/vs/editor/contrib/snippet/snippetController2';
import 'monaco-editor/esm/vs/editor/contrib/suggest/suggestController';
import 'monaco-editor/esm/vs/editor/contrib/tokenization/tokenization';
import 'monaco-editor/esm/vs/editor/contrib/toggleTabFocusMode/toggleTabFocusMode';
import 'monaco-editor/esm/vs/editor/contrib/wordHighlighter/wordHighlighter';
import 'monaco-editor/esm/vs/editor/contrib/wordOperations/wordOperations';
import 'monaco-editor/esm/vs/editor/contrib/wordPartOperations/wordPartOperations';
import 'monaco-editor/esm/vs/editor/standalone/browser/iPadShowKeyboard/iPadShowKeyboard';
//import 'monaco-editor/esm/vs/editor/standalone/browser/quickOpen/gotoLine';
//import 'monaco-editor/esm/vs/editor/standalone/browser/quickOpen/quickCommand';
//import 'monaco-editor/esm/vs/editor/standalone/browser/quickOpen/quickOutline';
import 'monaco-editor/esm/vs/editor/standalone/browser/referenceSearch/standaloneReferenceSearch';
import './index.scss';

import { editor, languages } from 'monaco-editor/esm/vs/editor/editor.api';

import { SRC } from '@utils/import-entry';

export * from 'monaco-editor/esm/vs/editor/editor.api';

/*@__INLINE__*/
function createWorker(url: string) {
    return new Worker(URL.createObjectURL(new Blob([`importScripts('${url}')`], { type: 'application/javascript' })));
}

self['MonacoEnvironment'] = {
    getWorker(_: any, label: string) {
        return createWorker(SRC + MONACO_WORKER_URLS[label === 'typescript' || label === 'javascript' ? 'typescript.worker' : 'editor.worker'].js);
    }
}

languages.typescript.javascriptDefaults.setDiagnosticsOptions({
    noSemanticValidation: false,
    noSyntaxValidation: false
});

languages.typescript.javascriptDefaults.setCompilerOptions({
    target: languages.typescript.ScriptTarget.ES2020,
    allowNonTsExtensions: true,
    module: languages.typescript.ModuleKind.CommonJS,
    moduleResolution: languages.typescript.ModuleResolutionKind.NodeJs,
    strictBindCallApply: true,
    resolveJsonModule: true,
    allowSyntheticDefaultImports: true,
    lib: ['es2020', 'node']
});

editor.defineTheme('cyr', {
    base: 'vs-dark',
    inherit: true,
    rules: [
        { token: 'comment', foreground: '7F848E', fontStyle: 'italic' },
        { token: 'number', foreground: 'E5C07B' },
        { token: 'identifier', foreground: 'ABB2BF' },
        { token: 'invalid', foreground: 'ABB2BF' },
        { token: 'delimiter.bracket.js', foreground: 'ABB2BF' },
        { token: 'delimiter.parenthesis.js', foreground: 'ABB2BF' },
        { token: 'keyword.js', foreground: 'C678DD' },
        { token: 'regexp.js', foreground: 'D19A66' },
        { token: 'string', foreground: '98C379' },
        { token: 'delimiter', foreground: '61AFEF' },
        { token: 'keyword.other.js', foreground: '61AFEF' }
    ],
    colors: {
        'activityBar.background': '#282c34',
        'activityBar.foreground': '#d7dae0',
        'activityBarBadge.background': '#4d78cc',
        'activityBarBadge.foreground': '#f8fafd',
        'badge.background': '#282c34',
        'button.background': '#404754',
        'debugToolBar.background': '#21252b',
        'diffEditor.insertedTextBackground': '#00809b33',
        'dropdown.background': '#21252b',
        'dropdown.border': '#21252b',
        'editor.background': '#282c34',
        'editor.findMatchBackground': '#42557b',
        'editor.findMatchBorder': '#457dff',
        'editor.findMatchHighlightBackground': '#6199ff2f',
        'editor.foreground': '#abb2bf',
        'editor.lineHighlightBackground': '#2c313c',
        'editorLineNumber.activeForeground': '#abb2bf',
        'editor.selectionBackground': '#67769660',
        'editor.selectionHighlightBackground': '#ffffff10',
        'editor.selectionHighlightBorder': '#dddddd',
        'editor.wordHighlightBackground': '#d2e0ff2f',
        'editor.wordHighlightBorder': '#7f848e',
        'editor.wordHighlightStrongBackground': '#abb2bf26',
        'editor.wordHighlightStrongBorder': '#7f848e',
        'editorActiveLineNumber.foreground': '#737984',
        'editorBracketMatch.background': '#515a6b',
        'editorBracketMatch.border': '#515a6b',
        'editorCursor.background': '#ffffffc9',
        'editorCursor.foreground': '#528bff',
        'editorError.foreground': '#c24038',
        'editorGroup.background': '#181a1f',
        'editorGroup.border': '#181a1f',
        'editorGroupHeader.tabsBackground': '#21252b',
        'editorHoverWidget.background': '#21252b',
        'editorHoverWidget.border': '#181a1f',
        'editorIndentGuide.activeBackground': '#c8c8c859',
        'editorIndentGuide.background': '#3b4048',
        'editorLineNumber.foreground': '#495162',
        'editorMarkerNavigation.background': '#21252b',
        'editorRuler.foreground': '#abb2bf26',
        'editorSuggestWidget.background': '#21252b',
        'editorSuggestWidget.border': '#181a1f',
        'editorSuggestWidget.selectedBackground': '#2c313a',
        'editorWarning.foreground': '#d19a66',
        'editorWhitespace.foreground': '#3b4048',
        'editorWidget.background': '#21252b',
        'focusBorder': '#464646',
        'input.background': '#1d1f23',
        'list.activeSelectionBackground': '#2c313a',
        'list.activeSelectionForeground': '#d7dae0',
        'list.focusBackground': '#383e4a',
        'list.highlightForeground': '#c5c5c5',
        'list.hoverBackground': '#292d35',
        'list.inactiveSelectionBackground': '#2c313a',
        'list.inactiveSelectionForeground': '#d7dae0',
        'list.warningForeground': '#d19a66',
        'menu.foreground': '#c8c8c8',
        'panelSectionHeader.background': '#21252b',
        'peekViewEditor.background': '#1b1d23',
        'peekViewEditor.matchHighlightBackground': '#29244b',
        'peekViewResult.background': '#22262b',
        'scrollbarSlider.activeBackground': '#747d9180',
        'scrollbarSlider.background': '#4e566660',
        'scrollbarSlider.hoverBackground': '#5a637580',
        'textLink.foreground': '#61afef',
    }
});