import Vue from 'vue';
import Component from 'vue-class-component';

import notify from '@utils/notify';

import type { editor, IDisposable } from 'monaco-editor';

@Component
export default class extends Vue.extend({
    props: {
        resizeEmmiter: Set,
        fullscreen: Boolean,
        value: String,
        readonly: Boolean,
        mode: String
    },
    watch: {
        linesCount() {
            this.onResize?.();
        },
        readonly(readOnly: boolean) {
            this.editor?.updateOptions({ readOnly });
        },
        value(v: string) {
            this.setText(v);
        }
    }
}) {
    $refs!: {
        container: HTMLElement
    }

    protected editor?: editor.IStandaloneCodeEditor;
    protected model?: editor.IModel;
    protected onDidChange?: IDisposable;
    protected onChange?: (e: editor.IModelContentChangedEvent) => void;
    protected onResize?: () => void;
    protected lastV?: string;

    protected linesCount = 0;

    setText(v: string) {
        v ||= '';
        if (this.model && v != this.lastV) {
            this.onDidChange?.dispose();
            const { model } = this;
            model.setValue(this.lastV = v);
            this.onDidChange = this.onChange ? model.onDidChangeContent(this.onChange) : null;
            this.linesCount = model.getLineCount();
        }
    }

    mounted() {
        try {
            const { editor, KeyCode, KeyMod } = window['monacoLib'] as typeof import('monaco-editor');

            const e = this.editor = editor.create(this.$refs.container, {
                fontSize: 13,
                fontFamily: "Consolas, 'Courier New', monaco, monospace",
                lineHeight: 16,
                language: this.mode || 'javascript',
                accessibilitySupport: 'off',
                autoClosingBrackets: 'never',
                minimap: { enabled: false },
                quickSuggestionsDelay: 200,
                renderIndentGuides: false,
                renderLineHighlight: 'gutter',
                renderWhitespace: 'none',
                roundedSelection: false,
                wordSeparators: '`~!@#%^&*()=-+[{]}\\|;:\'",.<>/?',
                wordWrap: 'on',
                lightbulb: { enabled: false },
                readOnly: this.readonly,
                scrollBeyondLastLine: false,
                theme: 'cyr'
            });
            e.addCommand(KeyMod.CtrlCmd | KeyMod.Shift | KeyCode.KEY_P, () => e.getAction('editor.action.quickCommand').run());
            e.addCommand(KeyMod.CtrlCmd | KeyCode.KEY_D, () => e.getAction('editor.action.formatDocument').run());

            const model = this.model = e.getModel();

            this.onChange = () => {
                this.linesCount = model.getLineCount();
                this.$emit('input', this.lastV = model.getValue());
            };

            this.resizeEmmiter.add(this.onResize = () => {
                const dim = (this.$refs.container as HTMLParagraphElement).getBoundingClientRect();
                e.layout({ width: dim.width - 2, height: this.fullscreen ? Math.max(16, dim.height - 20) : this.linesCount * 16 });
            });

            this.setText(this.value);
        } catch (err) {
            this.resizeEmmiter.delete(this.onResize);
            this.onDidChange?.dispose();
            this.editor?.dispose();
            notify(err);
        }
    }

    beforeDestroy() {
        this.resizeEmmiter.delete(this.onResize);
        this.onDidChange?.dispose();
        this.editor?.dispose();
    }

    render() {
        return <div class="monaco-editor-container" ref="container" style={{
            height: this.fullscreen ? '100%' : this.linesCount * 16 + 20 + 'px'
        }} />;
    }
}