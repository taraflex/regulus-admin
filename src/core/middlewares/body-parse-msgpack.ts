import rawBody from 'raw-body';

import { decodeBuffer } from '@utils/msgpack';

import type { Context } from 'koa';

export const MAX_PAYLOAD_SIZE = 1024 * 1024 * 5;

export default async (ctx: Context, next: () => Promise<any>) => {
    const { method } = ctx.request
    if (method === 'PATCH' || method === 'PUT' || method === 'POST') {
        const body = await rawBody(ctx.req, { limit: MAX_PAYLOAD_SIZE });
        ctx.request.body = body?.length ? Object.assign(decodeBuffer(body), ctx.request.query) : ctx.request.query;
    }
    return next();
}