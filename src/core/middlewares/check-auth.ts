import type { Context } from 'koa';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.isAuthenticated()) {
        return next();
    } else if (ctx.isAjax) {
        throw 401;
    } else {
        throw 'Login required'; //ctx.namedRedirect('login');
    }
}