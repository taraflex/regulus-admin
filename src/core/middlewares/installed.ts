import type { Context } from 'koa';
import type { Settings } from '@entities/Settings';

export default (settings: Settings) => function (ctx: Context, next: () => Promise<any>) {
    if (settings.installed) {
        return next();
    } else {
        ctx.namedRedirect('install');
    }
}