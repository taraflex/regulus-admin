import { stringify } from '@taraflex/string-tools';

import type { Context } from 'koa';

declare module 'koa' {
    interface Context {
        flash?: FlashData;
        addFlash?: (message: any, level?: FlashLevel) => void;
    }
}

const FLASH_COOKIE_LITERAL = '_f';

export default async (ctx: Context, next: () => Promise<any>) => {

    if (ctx.isAjax) {
        return next();
    } else {

        const t = ctx.getCookie(FLASH_COOKIE_LITERAL);
        let a: string[][] = [[], [], []];

        try {
            if (t) {
                ctx.flash = {
                    messages: t[FlashLevel.INFO],
                    warnings: t[FlashLevel.WARNING],
                    errors: t[FlashLevel.ERROR]
                };
            }

            ctx.addFlash = (message: any, level?: FlashLevel) => {
                a[level | 0].push(stringify(message));
            }

            await next();

        } finally {
            if (a.some(v => v.length > 0)) {
                ctx.setCookie(FLASH_COOKIE_LITERAL, a);
            } else if (t) {
                ctx.deleteCookie(FLASH_COOKIE_LITERAL);
            }
        }
    }
}