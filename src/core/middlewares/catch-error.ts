import normalizeHttpError from '@utils/normalize-http-error';

import type { Context } from 'koa';

export default async (ctx: Context, next: () => Promise<any>) => {
    try {
        await next();
    } catch (err) {
        const { status, body } = normalizeHttpError(err);
        ctx.status = status;
        ctx.body = body;
    }
}