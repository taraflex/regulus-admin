import type { Context } from 'koa';

export default function (ctx: Context, next: () => Promise<any>) {
    if (ctx.method === 'GET') {
        const { path } = ctx;
        if (path !== '/' && path.endsWith('/')) {
            ctx.status = 301;
            return ctx.redirect(path.slice(0, -1) + (ctx.querystring ? '?' + ctx.querystring : ''));
        }
    }
    return next();
}