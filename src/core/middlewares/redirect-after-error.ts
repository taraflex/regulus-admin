import { destroySession } from 'core/AuthRegister';

import normalizeHttpError from '@utils/normalize-http-error';

import type { Context } from 'koa';

export default (redirectRouteName: string) => async function (ctx: Context, next: () => Promise<any>) {
    if (ctx.isAjax) {
        await next();
    } else {
        try {
            await next();
        } catch (err) {
            const { status, body } = normalizeHttpError(err);

            ctx.status = status;

            body.errors.forEach(e => ctx.addFlash(e));

            if (status >= 500) {
                destroySession(ctx);
                ctx.namedRedirect(redirectRouteName, 1);
            } else {
                ctx.namedRedirect(redirectRouteName);
            }
        }
    }
}