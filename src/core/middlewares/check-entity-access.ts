import type { Context } from 'koa';
import type { Hook, EntityClass } from '@crud';

export default ({ checkAccess }: EntityClass) => function (ctx: Context, next: () => Promise<any>) {
    if (!ctx.isAjax) {
        throw 403;
    }
    checkAccess(ctx.request.method as Hook, ctx.state.user);
    return next();
}