import { DEFAULT_COOKIE_OPTIONS, Koa } from 'core/Koa';
import { Context, Middleware } from 'koa';
import compose from 'koa-compose';
import passport from 'koa-passport';
import session from 'koa-session';
import { Strategy } from 'passport-local';
import { verify } from 'scrypt-kdf';
import { Inject, Singleton } from 'typescript-ioc';

import { User, UserRepository } from '@entities/User';
import { destroyAllConnections, destroyBrowserConnections } from '@rpc/ServerRPC';
import { trim } from '@taraflex/string-tools';

export const SESSION_COOKIE_LITERAL = '_s';

export function destroySession(ctx: Context) {
    ctx.res.once('finish', () => destroyBrowserConnections(ctx));
    ctx.session = null;
}

export function destroyAllSessions() {
    //todo rotate koa secret
    destroyAllConnections();
}

@Singleton
export class AuthRegister {
    readonly session: Middleware;
    readonly passport: Middleware;
    readonly passportSession: Middleware;
    readonly passportComposed: Middleware;

    constructor(
        @Inject userRepository: UserRepository,
        @Inject app: Koa
    ) {

        passport.serializeUser((user: User, done: Function) => done(null, user.id));

        passport.deserializeUser((id: number, done) => userRepository.findOneOrFail(id)
            .then(user => done(null, user))
            .catch(done)
        );

        passport.use(new Strategy({
            usernameField: 'email',
            passwordField: 'password'
        }, async (email: string, password: string, done: Function) => {
            try {
                email = trim(email);
                password = trim(password);
                const user = await userRepository.findOne({ email });
                if (!user) {
                    throw 'User not found';
                }
                if (!await verify(Buffer.from(user.password, 'base64'), password)) {
                    throw 'Wrong password';
                }
                done(null, user);
            } catch (err) {
                done(err);
            }
        }));

        this.passportComposed = compose([

            this.session = session({
                ...DEFAULT_COOKIE_OPTIONS,
                key: SESSION_COOKIE_LITERAL,
                maxAge: 86400000 * 365,
                renew: true,
                signed: true
            }, app),

            this.passport = passport.initialize(),

            this.passportSession = passport.session()

        ]);
    }
}