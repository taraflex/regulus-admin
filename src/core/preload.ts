import { spawn } from 'child_process';
import { bootstrap } from 'global-agent';

import { DEBUG, PEM, PORT, REMOTE_HOSTNAME } from '@utils/config';
import { DEV_NULL, PROXY_URL } from '@utils/proxy-info';

if (!DEBUG) {
    const PrettyError = require('pretty-error');
    const pe = new PrettyError();
    pe.withoutColors();//в логи попадают управляющие символы отвечающие за цвет - становится нечитабельно
    pe.start();
}

if (PEM) {
    (global as any).SSH_TUNNEL = spawn('ssh', ['-o', 'UserKnownHostsFile=' + DEV_NULL, '-o', 'StrictHostKeyChecking=no', '-o', 'BatchMode=yes', '-gnNT', '-R', PORT + ':localhost:' + PORT, '-i', PEM, 'root@' + REMOTE_HOSTNAME], {
        windowsHide: true,
        stdio: 'inherit'
    }).once('exit', code => process.exit(code));

    bootstrap();
    // @ts-ignore
    global.GLOBAL_AGENT.NO_PROXY = 'localhost,127.0.0.1,ipecho.net,ident.me,icanhazip.com';
    // @ts-ignore
    global.GLOBAL_AGENT.HTTP_PROXY = PROXY_URL;

    LOG_INFO('Active proxy url: ' + PROXY_URL);
} else {
    LOG_INFO('Proxy url: ' + PROXY_URL);
}