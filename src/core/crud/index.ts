import 'reflect-metadata';

import {
    ColumnOptions, Connection, getMetadataArgsStorage, ObjectLiteral, Repository
} from 'typeorm';
import { Provider } from 'typescript-ioc';

import { has } from '@crud/helpers';
import { isValidVar } from '@taraflex/string-tools';
import { compile } from '@utils/validator-helpers';

export interface ParametricProvider<T, Args extends any[]> extends Provider {
    init(...args: Args): T;
}

export interface AsyncProvider<T, Args extends any[]> extends Provider {
    init(...args: Args): Promise<T>;
}

export type ValidatorType = 'files' | 'link' | 'number' | 'string' | 'boolean' | 'array' | 'object' | 'wysiwyg' | 'md-tg' | 'regex' | 'javascript' | 'date' | 'enum' | 'any';

export type ArrayValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty array[].
     */
    empty?: boolean,
    /**
     *Minimum count of elements.
     */
    min?: number,
    /**
     *Maximum count of elements.
     */
    max?: number,
    /**
     *Fix count of elements.
     */
    length?: number,
    /**
     *The array must contains this element too.
     */
    contains?: any,
    /**
     *Every element must be an element of the enum array.
     */
    enum?: any[],
    items?: Validator | Validator[]
}

export type AnyValidator = {
    type?: ValidatorType
}

export type BooleanValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Boolean, try to convert. 1, "true", "1", "on" will be true. 0, "false", "0", "off" will be false.
     */
    convert?: boolean
}

export type DateValidator = {
    type?: ValidatorType,
    /**
     *if true and the type is not Date, try to convert with new Date().
     */
    convert?: boolean
}

export type NumberValidator = {
    type?: ValidatorType,
    /**
     *Minimum value.
     */
    min?: number,
    /**
     *Maximum value.
     */
    max?: number,
    /**
     *Fix value.
     */
    equal?: number,
    /**
     *Can't be equal with this value.
     */
    notEqual?: number,
    /**
     *The value must be a non - decimal value.
     */
    integer?: boolean,
    /**
     *The value must be larger than zero.
     */
    positive?: boolean,
    /**
     *The value must be less than zero.
     */
    negative?: boolean,
    /**
     *if true and the type is not Number, try to convert with parseFloat.
     */
    convert?: boolean
}

export type ObjectValidator = {
    type?: ValidatorType,
    props: { [property: string]: Validator | Validator[] }
}

export type StringValidator = {
    type?: ValidatorType,
    /**
     *If true, the validator accepts empty string "".
     */
    empty?: boolean,
    /**
     *Minimum length of value.
     */
    min?: number,
    /**
     *Maximum length of value.
     */
    max?: number,
    /**
     *Fix length of value.
     */
    length?: number,
    /**
     *Regex pattern.
     */
    pattern?: RegExp,
    /**
     *The value must contains this text.
     */
    contains?: string,
    /**
     *The value must be an element of the enum array.
     */
    enum?: any[]
}

export type JavascriptValidator = StringValidator & {
    typeDefinitions?: string
}

export type Validator = ArrayValidator | DateValidator | AnyValidator | /*ForbiddenValidator |*/ BooleanValidator | NumberValidator | ObjectValidator | StringValidator | JavascriptValidator;

export type AdditionalProperties = {
    description?: string,
    placeholder?: string,
    remote?: string;
    uploadUrl?: string;
    uploadAccept?: string;
    state?: number;
}

export type RTTIItem = Validator & AdditionalProperties & {
    type: ValidatorType,
    optional: boolean,//for validators
    state: number,
    default?: any,
    enumPairs?: any[][],
}

export type DisplayInfoType = 'single' | 'table';

export interface DisplayInfo {
    icon?: string;
    display?: DisplayInfoType;
    order?: number;
    sortable?: boolean;
}

export interface RTTI<T = ObjectLiteral> {
    displayInfo: DisplayInfo;
    props: Record<keyof T, RTTIItem>;
}

export type Hook = 'GET' | 'PATCH' | 'PUT' | 'DELETE';
export type Can = Hook | 'CHANGE';
type Action = Hook | '_';

export interface EntityClass<E = any, Args extends any[] = [Connection]
    > extends Function {
    new(): E;
    asyncProvider?: AsyncProvider<E, Args>;
    insertValidate?: (o: E) => void;
    updateValidate?: (o: E) => void;
    checkAccess?: (action: Action, user: { role: number }) => true;
    rtti?: RTTI<E>;
    hooks?: { [_ in Hook]?: string };
    __validatorInfo?: { [property: string]: Validator & AdditionalProperties };
}

export interface RepositoryClass<E = any, Args extends any[] = [Connection]> extends Function {
    provider?: ParametricProvider<Repository<E>, Args>;
}

function nullPrune(o: RTTIItem) {
    for (const k in o) {
        if (o[k] == null) {
            delete o[k];
        }
    }
    return o;
}

function getFieldSchema(target: EntityClass, options: ColumnOptions, propertyName: string): RTTIItem {
    const { nullable, unsigned, array, zerofill, primary } = options;
    let type: ValidatorType;
    try {
        type = <any>(options.type as Function).name.toLowerCase();
    } catch {
        type = <any>options.type.toString();
    }

    let vo = target.__validatorInfo && target.__validatorInfo[propertyName];

    const _enum = options.enum || (vo as ArrayValidator)?.enum;

    let enumPairs: any[][] = undefined;
    let enumValues: any[] = undefined;

    const generated = getMetadataArgsStorage().generations.some(v =>
        (v.target === target || target.prototype instanceof <any>v.target) && //todo ckeck all classes tree
        v.propertyName === propertyName
    );

    if (Array.isArray(_enum)) {
        enumValues = _enum;
        enumPairs = enumValues.map(v => [v, v]);
    } else if (_enum) {
        const keys = new Set(Object.keys(_enum));
        while (keys.size > 0) {
            const { value } = keys.values().next();
            let k: string = value;
            let v = _enum[k];
            if (
                !isValidVar(k) ||
                (_enum[v] === k && isValidVar(v) && v.toUpperCase() == v && k.toUpperCase() !== k) //v constant case and k not
            ) {
                keys.delete(k);
                continue;
            }

            if (!Array.isArray(enumPairs)) {
                enumPairs = [];
                enumValues = [];
            }

            keys.delete(v.toString());
            keys.delete(k);
            enumPairs.push([k, v]);
            enumValues.push(v);
        }
    }

    let integer = undefined;

    if (type.includes('int')) {
        type = 'number';
        integer = true;
    } else if (type.includes('date') || type.includes('time')) {
        type = 'date';
    } else if (type.includes('string') || type.includes('char') || type.includes('clob') || type.includes('text')) {
        type = 'string';
    } else if (type.includes('bool')) {
        type = 'boolean';
    } else if (type.includes('blob') || type.includes('binary')) {
        throw 'Validator not implemented';
    } else if (<any>type === 'simple-array' || type === 'array' || array || (<any>type === 'simple-json' && !options.default?.startsWith?.('{'))) {
        type = 'array';
    } else if (type === 'enum') {
        //todo simple-enum
    } else {
        type = 'number';
    }

    if (!Array.isArray(enumValues) || enumValues.length < 1) {
        enumValues = enumPairs = undefined;
    }

    let state = vo ? vo.state | 0 : 0;

    if (type === 'array' || type === 'enum' || enumValues) {
        if (//todo check if allow multiple
            (state & RTTIItemState.ALLOW_CREATE) === RTTIItemState.ALLOW_CREATE ||
            !enumValues ||
            (vo && (vo as ArrayValidator).min > 1)
        ) {
            enumValues = null;
            type = 'array';
        } else {
            type = 'enum';
        }
        if (vo) {
            vo = { ...vo };
            delete (vo as ArrayValidator).enum;
        }
    }

    const isNumber = type === 'number';

    const o: RTTIItem = {
        type,
        enumPairs,
        enum: enumValues,
        // @ts-ignore
        values: enumValues, //for fastest-validator
        integer,
        min: isNumber && (unsigned || zerofill || (vo && (vo as NumberValidator).positive)) ? 0 : null,
        max: isNumber && vo && (vo as NumberValidator).negative ? 0 : null,
        optional: !generated && (nullable || (options.default != null)),
        default: zerofill && isNumber && options.default == null ? 0 : options.default,
        state
    }
    const r = nullPrune(vo ? Object.assign(o, vo) : o);
    if (r.type === 'link') {
        r.state |= RTTIItemState.READONLY;
    }
    if (primary) {
        r.state |= RTTIItemState.PRIMARY;
    }
    if (generated) {
        r.state |= RTTIItemState.HIDDEN;
    }
    return r;
}

function validate(o) {
    const r = this(o);
    if (r !== true) {
        throw r;
    }
}

function* getColumns(entity: EntityClass) {
    if (entity && entity !== Function.prototype) {
        yield* getColumns(Object.getPrototypeOf(entity));
        yield* getMetadataArgsStorage().filterColumns(entity);
    }
}

export function Access(allowedFor?: { [_ in Action]?: number }, displayInfo?: DisplayInfo, hooks?: { [_ in Hook]?: string }) {
    return (entity: EntityClass) => {
        const vInsert = Object.create(null);
        const vUpdate = Object.create(null);

        for (const { propertyName, options } of getColumns(entity)) {
            const schema = getFieldSchema(entity, options, propertyName);
            if (!has(schema, RTTIItemState.READONLY)) {
                vInsert[propertyName] = schema;
            }
            vUpdate[propertyName] = schema;
        }

        allowedFor ||= Object.create(null);
        if (allowedFor._ !== +allowedFor._) {
            allowedFor._ = -1;
        }
        Object.freeze(allowedFor);

        Object.defineProperty(entity, 'checkAccess', {
            value(action: Action, user: { role: number }) {
                if (!user) {
                    throw 401;
                }
                let t = allowedFor[action];
                if (t == null) {
                    t = allowedFor._;
                }
                if ((t & user.role) !== t) {
                    throw 403;
                }
                return true;
            }
        });

        for (const _ in vInsert) {
            Object.defineProperty(entity, 'insertValidate', {
                value: validate.bind(compile(vInsert))
            });
            break;
        }

        for (const _ in vUpdate) {
            Object.defineProperty(entity, 'updateValidate', {
                value: validate.bind(compile(vUpdate))
            });
            break;
        }

        displayInfo ||= Object.create(null);
        displayInfo.display ||= entity.asyncProvider ? 'single' : 'table';

        Object.defineProperty(entity, 'rtti', {
            value: Object.freeze({
                props: Object.freeze(vUpdate),
                displayInfo: Object.freeze(displayInfo)
            })
        });

        Object.defineProperty(entity, 'hooks', {
            value: hooks || Object.create(null)
        });

        delete entity.__validatorInfo;

        return entity;
    }
}

export function v(validatorProps: Validator & AdditionalProperties) {
    return ({ constructor }: { constructor: Function }, prop: string | symbol): void => {
        ((constructor as EntityClass).__validatorInfo || ((constructor as EntityClass).__validatorInfo = Object.create(null)))[prop] = Object.freeze(validatorProps);
    };
}