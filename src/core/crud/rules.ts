type File = { filename: string, path: string, ready?: boolean };
type Link = { title: string, url: URL };

/**
 * Union type of all possible built-in validators
 */
export type ValidationRuleObject<T = any> = T extends (infer A)[] ? RuleArray<A> : RuleArray<T>
    | RuleFiles
    | RuleBoolean
    | RuleDate
    | RuleEnum<T>
    | RuleLuhn
    | RuleMac
    | RuleNumber
    | RuleString
    | RuleJavascript
    | RuleLink
    | RuleFile
    | RuleRegExp
    | RuleUUID
    | RuleURL;

type PropType<TObj, TProp extends keyof TObj> = TObj[TProp];

export type ValidatorType = PropType<ValidationRuleObject, 'type'>;

export interface RuleURL extends RuleCustom<URL> {
    type?: 'url'
}

/**
 * Validation schema definition for "array" built-in validator
 * @see https://github.com/icebob/fastest-validator#array
 */
export interface RuleArray<T> extends RuleCustom<T[]> {
    /**
     * Name of built-in validator
     */
    type?: "array";
    /**
     * If true, the validator accepts an empty array [].
     * @default true
     */
    empty?: boolean;
    /**
     * Minimum count of elements
     */
    min?: number;
    /**
     * Maximum count of elements
     */
    max?: number;
    /**
     * Fixed count of elements
     */
    length?: number;
    /**
     * The array must contain this element too
     */
    contains?: T[];
    /**
     * Every element must be an element of the enum array
     */
    enum?: T[];
    /**
     * Validation rules that should be applied to each element of array
     */
    items?: ValidationRuleObject<T>;
}

export interface RuleFiles extends Omit<RuleArray<File>, 'type' | 'items'> {
    type: 'files'
}

/**
 * Validation schema definition for "boolean" built-in validator
 * @see https://github.com/icebob/fastest-validator#boolean
 */
export interface RuleBoolean extends RuleCustom<boolean> {
    /**
     * Name of built-in validator
     */
    type?: "boolean";
}

/**
 * Validation schema definition for "date" built-in validator
 * @see https://github.com/icebob/fastest-validator#date
 */
export interface RuleDate extends RuleCustom<Date> {
    /**
     * Name of built-in validator
     */
    type?: "date";
}

/**
 * Validation schema definition for "enum" built-in validator
 * @see https://github.com/icebob/fastest-validator#enum
 */
export interface RuleEnum<T> extends RuleCustom<T> {
    /**
     * Name of built-in validator
     */
    type: "enum";
    /**
     * The valid values
     */
    enum?: T[];
}

/**
 * Validation schema definition for "luhn" built-in validator
 * @see https://github.com/icebob/fastest-validator#luhn
 */
export interface RuleLuhn extends RuleCustom<string> {
    /**
     * Name of built-in validator
     */
    type: "luhn";

    mask?: string;
}

/**
 * Validation schema definition for "mac" built-in validator
 * @see https://github.com/icebob/fastest-validator#mac
 */
export interface RuleMac extends RuleCustom<string> {
    /**
     * Name of built-in validator
     */
    type: "mac";
}

/**
 * Validation schema definition for "number" built-in validator
 * @see https://github.com/icebob/fastest-validator#number
 */
export interface RuleNumber extends RuleCustom<number> {
    /**
     * Name of built-in validator
     */
    type?: "number";
    /**
    * The value must be a non-decimal value
    * @default false
    */
    integer?: boolean;
    /**
    * Minimum value
    */
    min?: number;
    /**
     * Maximum value
     */
    max?: number;
    /**
     * Fixed value
     */
    equal?: number;
    /**
     * Can't be equal to this value
     */
    notEqual?: number;
    /**
     * The value must be greater than zero
     * @default false
     */
    positive?: boolean;
    /**
     * The value must be less than zero
     * @default false
     */
    negative?: boolean;
}

export interface RuleBasicString extends RuleCustom<string> {
    /**
     * If true, the validator accepts an empty string ""
     * @default true
     */
    empty?: boolean;
    /**
     * Minimum value length
     */
    min?: number;
    /**
     * Maximum value length
     */
    max?: number;
    /**
     * Fixed value length
     */
    length?: number;
}

/**
 * Validation schema definition for "string" built-in validator
 * @see https://github.com/icebob/fastest-validator#string
 */
export interface RuleString extends RuleBasicString {
    /**
     * Name of built-in validator
     */
    type?: "string" | 'wysiwyg' | 'md-tg'
    /**
     * Regex pattern
     */
    pattern?: string | RegExp;
    /**
     * The value must contain this text
     */
    contains?: string[];
    /**
     * The value must be an alphabetic string
     */
    numeric?: boolean;
    /**
     * The value must be a numeric string
     */
    alpha?: boolean;
    /**
     * The value must be an alphanumeric string
     */
    alphanum?: boolean;
    /**
     * The value must be an alphabetic string that contains dashes
     */
    alphadash?: boolean;
    /**
     * The value must be a hex string
     * @default false
     */
    hex?: boolean;
    /**
     * The value must be a singleLine string
     * @default false
     */
    singleLine?: boolean;
    /**
     * The value must be a base64 string
     * @default false
     */
    base64?: boolean;
}

export interface RuleJavascript extends RuleBasicString {
    type: 'javascript',
    typeDefinitions?: string
}

export interface RuleLink extends RuleCustom<Link> {
    type?: 'link'
}

export interface RuleFile extends RuleCustom<File> {
    type?: 'file'
}

export interface RuleRegExp extends RuleCustom<RegExp> {
    type?: 'regexp'
}

/**
 * Validation schema definition for "uuid" built-in validator
 * @see https://github.com/icebob/fastest-validator#uuid
 */
export interface RuleUUID extends RuleCustom<string> {
    /**
     * Name of built-in validator
     */
    type: "uuid";
    /**
     * UUID version in range 0-6
     */
    version?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
}

/**
 * Validation schema definition for custom validator
 * @see https://github.com/icebob/fastest-validator#custom-validator
 */
export interface RuleCustom<D> {
    /**
     * Every field in the schema will be required by default. If you'd like to define optional fields, set optional: true.
     * @default false
     */
    optional?: boolean;

    /**
     * If you want disallow `undefined` value but allow `null` value, use `nullable` instead of `optional`.
     * @default false
     */
    nullable?: boolean;

    /**
     * Default value
     */
    default?: D;

    description?: string,
    placeholder?: string,
    remote?: string;
    state?: RTTIItemState;
    fullwidth?: boolean;//todo allow only for types that can be fullwidth

    /**
    * Specifies if this column will use auto increment (sequence, generated identity, rowid).
    * Note that in some databases only one column in entity can be marked as generated, and it must be a primary column.
    */
    generated?: boolean | "increment" | "uuid" | "rowid";

    /**
     * Indicates if this column is a primary key.
     * Same can be achieved when @PrimaryColumn decorator is used.
     */
    primary?: boolean;

    /**
     * Specifies if column's value must be unique or not.
     */
    uniqueCol?: boolean;
}
