import FastestValidator, { CompilationRule, Context, ValidationSchema } from 'fastest-validator';

import { stringify } from '@taraflex/string-tools';
import deepClone from '@utils/deep-clone';

import type { ValidationRuleObject } from '@crud/rules';

/*@__INLINE__*/
export function has(item: { state?: RTTIItemState }, state: RTTIItemState) {
    return (item.state & state) === state;
}

export function findPrimaryField<T = { [_: string]: any }>(props: Record<keyof T, any>) {
    for (const k in props) {
        if (has(props[k], RTTIItemState.PRIMARY)) {
            return k;
        }
    }
    throw new Error("Can't find primary field");
}

export const URL_TRANSFORMER = Object.freeze({
    to(url: URL) { return url.href },
    from(href: string) { return new URL(href) }
});

export const REGEXP_TRANSFORMER = Object.freeze({
    to(regex: RegExp) { return JSON.stringify([regex.source, regex.flags]) },
    from(data: string) {
        try {
            return new RegExp(...JSON.parse(data) as [string, string])
        } catch (err) {
            if (err instanceof SyntaxError) {
                return new RegExp(data);
            }
            throw err;
        }
    }
});

export function typeDefault(v: ValidationRuleObject) {
    if (v.default != null) {
        return deepClone(v.default);
    } else if (v.nullable) {
        return null;
    }
    switch (v.type) {
        case 'number':
            return 0;
        case 'array':
            return [];
        case 'javascript':
        case 'string':
        case 'wysiwyg':
        case 'md-tg':
            return '';
        case 'luhn': return '00';
        case 'file': return { path: null, filename: '', ready: false };
        case 'realurl': return new URL('http://localhost');
        case 'link': return { url: new URL('http://localhost'), title: '' };
        case 'regexp': return new RegExp('');
        case 'enum': return v.enum?.[0] || null;
        case 'boolean': return false;
        case 'date': return new Date();
        case 'uuid': return '00000000-0000-0000-0000-000000000000';
        case 'mac': return '00:00:00:00:00:00';
    }
    throw new TypeError('Unsupported property type: ' + stringify(v));
}

function makeRule(template: (schema: ValidationSchema) => string, basicRuleName?: string) {
    return function (rule: CompilationRule, path: string, context: Context) {
        const r = basicRuleName ? (FV.rules[basicRuleName] as Function).call(this, rule, path, context) : null;
        return {
            sanitized: basicRuleName ? r.sanitized : false,
            source: `
try {
${template(rule.schema)};
} catch(err) {
${this.makeError({ type: "simple", actual: "err.message", messages: rule.messages })}
}
` + (basicRuleName ? r.source : 'return value;')
        };
    };
}

export const FV = new FastestValidator({
    messages: {
        simple: "The '{field}' {actual}"
    },
    customRules: {
        javascript: makeRule(_ => 'new Function(value)', 'string'),
        //regex: makeRule(schema => `new RegExp(value, '${schema.flags || ''}')`),
    },
    aliases: {
        'regexp': { type: 'class', instanceOf: RegExp },
        'file': {
            type: 'object',
            props: {
                path: { type: 'string', empty: false },
                filename: { type: 'string', empty: false },
                ready: { type: 'equal', value: true, optional: true }
            }
        },
        'realurl': { type: 'class', instanceOf: URL },
        'link': {
            type: 'object',
            props: {
                title: { type: 'string' },
                url: { type: 'realurl' },
            }
        },
        'wysiwyg': { type: 'string' },
        'md-tg': { type: 'string' }
    }
});