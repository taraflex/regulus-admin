import { destroyAllSessions, destroySession } from 'core/AuthRegister';
import bodyParser from 'koa-body';
import passport from 'koa-passport';

import checkAuth from '@middlewares/check-auth';
import { get, post, put } from '@utils/routes-helpers';

import type { Context } from 'koa';

export class AuthRoutes {

    @get({ name: 'login' })
    login(ctx: Context) {
        ctx.pug('login');
    }

    @post({ path: '/login' }, bodyParser({
        text: false,
        json: false
    }))
    loginPost(ctx: Context, next: () => Promise<any>) {
        return passport.authenticate('local', async (err, user, info) => {
            if (err) {
                ctx.addFlash(err);
            }
            if (info?.message) {
                ctx.addFlash(info.message, FlashLevel.WARNING);
            }
            if (user) {
                await ctx.login(user);
                ctx.namedRedirect('root');
            } else {
                ctx.namedRedirect('login');
            }
        })(ctx, next);
    }

    @put({ name: 'logout' }, checkAuth)
    logout(ctx: Context) {
        ctx.status = 204;
        destroySession(ctx);
    }

    @put({ name: 'logoutAll' }, checkAuth)
    logoutAll(ctx: Context) {
        ctx.status = 204;
        destroyAllSessions();
    }
}