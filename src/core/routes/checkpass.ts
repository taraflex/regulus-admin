import bodyParser from 'koa-body';
import { verify } from 'scrypt-kdf';
import { Singleton } from 'typescript-ioc';

import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import { get, post } from '@utils/routes-helpers';

import type { Context } from 'koa';

@Singleton
export class CheckPassRoutes {

    @get({ name: 'checkpass', path: '/checkpass/:action' })
    checkpass(ctx: Context) {
        const { action } = ctx.params;
        ctx.pug('checkpass', { action: ctx.resolve('checkpass', { action }) });
    }

    @post({ path: '/checkpass/:action' }, redirectAfterError('checkpass'), bodyParser({
        text: false,
        json: false
    }))
    async checkpassPost(ctx: Context) {
        ctx.session.confirmed = 0;
        let { password } = ctx.request.body;

        if (!await verify(Buffer.from(ctx.state.user.password, 'base64'), trim(password))) {
            throw 'Wrong password';
        }
        ctx.session.confirmed = Date.now();
        ctx.namedRedirect(ctx.params.action);
    }

}