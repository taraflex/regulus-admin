import bodyParser from 'koa-body';
import { Inject, Singleton } from 'typescript-ioc';

import { UserRepository } from '@entities/User';
import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import { get, post } from '@utils/routes-helpers';

import type { Context } from 'koa';

@Singleton
export class ResetEmailRoutes {

    constructor(@Inject private readonly userRepository: UserRepository) { }

    @get({ name: 'resetemail' })
    resetemail(ctx: Context) {
        if (Date.now() - (ctx.session.confirmed || 0) < 60000 * 2) {
            ctx.pug('resetemail');
        } else {
            ctx.namedRedirect('checkpass', 0, { action: 'resetemail' });
        }
    }

    @post({ path: '/resetemail' }, redirectAfterError('resetemail'), bodyParser({
        text: false,
        json: false
    }))
    async resetemailPost(ctx: Context) {
        if (Date.now() - (ctx.session.confirmed || 0) >= 60000 * 2) {
            throw 'Expiried password confirmation';
        }
        let { email } = ctx.request.body;

        const user = { ...ctx.state.user, email: trim(email) };

        await this.userRepository.save(user);
        await ctx.login(user);
        ctx.namedRedirect('root');
    }
}