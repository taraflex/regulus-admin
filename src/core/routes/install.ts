import bodyParser from 'koa-body';
import { kdf } from 'scrypt-kdf';
import { Inject } from 'typescript-ioc';

import { Settings, SettingsRepository } from '@entities/Settings';
import { User, UserRepository } from '@entities/User';
import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import { get, post } from '@utils/routes-helpers';

import type { Context } from 'koa';
import type { EntityClass } from '@crud';

export class InstallRoutes {
    constructor(
        @Inject private readonly settings: Settings,
        @Inject private readonly userRepository: UserRepository,
        @Inject private readonly settingsRepository: SettingsRepository
    ) { }

    @get({ name: 'install' })
    install(ctx: Context) {
        if (this.settings.installed) {
            if (ctx.isAuthenticated()) {
                ctx.namedRedirect('root');
            } else {
                ctx.addFlash('Already installed. Login please.', FlashLevel.WARNING);
                ctx.namedRedirect('login');
            }
        } else {
            ctx.pug('install');
        }
    }

    @post({ path: '/install' }, redirectAfterError('install'), bodyParser({
        text: false,
        json: false
    }))
    async installPost(ctx: Context) {
        if (this.settings.installed) {
            ctx.namedRedirect('install');
        } else {
            let { email, password } = ctx.request.body;
            email = trim(email);
            password = trim(password);

            (User as EntityClass<User>).validate({ email, password });

            let user = await this.userRepository.findOne({ email });
            if (user) {
                throw 'Пользователь с таким email уже существует.';
            }

            user = this.userRepository.create({
                email,
                password: (await kdf(password, { logN: 15, r: 8, p: 1 })).toString('base64')
            });
            await this.userRepository.insert(user);

            this.settings.installed = true;
            await this.settingsRepository.save(this.settings);

            await ctx.login(user);
            ctx.namedRedirect('root');
        }
    }
}