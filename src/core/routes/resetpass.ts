import bodyParser from 'koa-body';
import { kdf } from 'scrypt-kdf';
import { setInterval } from 'timers';
import { Inject, Singleton } from 'typescript-ioc';

import { User, UserRepository } from '@entities/User';
import redirectAfterError from '@middlewares/redirect-after-error';
import { trim } from '@taraflex/string-tools';
import detectEmailService from '@utils/detect-email-service';
import { rndString } from '@utils/rnd';
import { get, post } from '@utils/routes-helpers';
import sendEmail from '@utils/send-email';

import type { Context } from 'koa';
import type { EntityClass } from '@crud';

const RESET_PASSWORD_TIMEOUT = 60000 * 20;

@Singleton
export class ResetPassRoutes {
    private readonly resetPasswordTokens: { [token: string]: { id: number, time: number } } = Object.create(null);

    constructor(@Inject private readonly userRepository: UserRepository) {
        setInterval(() => {
            const NOW = Date.now();
            for (const token in this.resetPasswordTokens) {
                if (NOW - this.resetPasswordTokens[token].time > RESET_PASSWORD_TIMEOUT) {
                    delete this.resetPasswordTokens[token];
                }
            }
        }, RESET_PASSWORD_TIMEOUT).unref();
    }

    validateToken(token: string) {
        let tokenInfo = this.resetPasswordTokens[token];
        if (!tokenInfo) {
            throw 'Invalid or expiried recovery url';
        }
        const { time } = tokenInfo;
        if (Date.now() - time > RESET_PASSWORD_TIMEOUT) {
            delete this.resetPasswordTokens[token];
            throw 'Expiried recovery url';
        }
        return tokenInfo;
    }

    @get({ name: 'resetpass' })
    resetpass(ctx: Context) {
        ctx.pug('resetpass', { email: ctx.isAuthenticated() ? ctx.state.user.email : '' });
    }

    @post({ path: '/resetpass' }, redirectAfterError('resetpass'), bodyParser({
        text: false,
        json: false
    }))
    async resetpassPost(ctx: Context) {
        let { email } = ctx.request.body;
        email = trim(email);

        const user = await this.userRepository.findOne({ email });
        if (!user) {
            throw 'User not found';
        }

        const { id } = user;
        for (const t in this.resetPasswordTokens) {
            if (this.resetPasswordTokens[t].id === id) {
                delete this.resetPasswordTokens[t];
            }
        }
        const token = await rndString();
        this.resetPasswordTokens[token] = { id, time: Date.now() };

        await sendEmail(email, 'Изменение пароля ' + ctx.resolve('root'), 'Для продолжения смены пароля перейдите на ' + ctx.resolve('newpassword', { token }));

        ctx.pug('resetpass-sended', { email, service: detectEmailService(email) });
    }

    @get({ name: 'newpassword', path: '/newpassword/:token' }, redirectAfterError('resetpass'))
    newpassword(ctx: Context) {
        const { token } = ctx.params;

        this.validateToken(token);

        ctx.pug('newpassword', {
            action: ctx.resolve('newpassword', { token })
        });
    }

    @post({ path: '/newpassword/:token' }, redirectAfterError('resetpass'), bodyParser({
        text: false,
        json: false
    }))
    async newpasswordPost(ctx: Context) {
        const { token } = ctx.params;
        const { id } = this.validateToken(token);

        delete this.resetPasswordTokens[token];

        const user = id && await this.userRepository.findOne(id);
        if (!user) {
            throw 'User not found';
        }

        let { password } = ctx.request.body;
        password = trim(password);

        (User as EntityClass<User>).validate({
            email: user.email,
            password
        });

        user.password = (await kdf(password, { logN: 15, r: 8, p: 1 })).toString('base64');
        await this.userRepository.save(user);
        await ctx.login(user);
        ctx.namedRedirect('root');
    }
}