export default function <K, V>(m: Map<K, V>, s: number) {
    let o = m.size - s;
    if (o > 0) {
        for (const k of m.keys()) {
            m.delete(k);
            if (!--o) {
                break;
            }
        }
    }
}