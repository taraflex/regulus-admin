import statuses from 'builtin-status-codes';

import { isJsonable, stringify } from '@taraflex/string-tools';

function convertError(err: any) {
    return isJsonable(err) ? err : stringify(err);
}

export default function (err: any) {
    if (Array.isArray(err)) {
        return { status: 400, body: { errors: err.map(convertError) } };
    } else {
        const status = (+err | 0) || (+err.status | 0) || 500;
        if (status >= 500) {
            LOG_ERROR(err);
        }
        if (statuses[+err] || status >= 500) {
            err = statuses[status] || statuses[500];
        }
        return { status, body: { errors: [convertError(err)] } };
    }
}