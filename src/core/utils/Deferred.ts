import { ExtendableError } from '@utils/extendable-error';
import rndId from '@utils/rnd-id';

class DefferTimeout extends ExtendableError { }
class DefferError extends ExtendableError { }

export type IDeferred<T = any> = {
    expires?: number,
    timeoutMessage?: string,
    promise: Promise<T>,
    timer?: ReturnType<typeof setTimeout>,
    fulfill: (v: T | PromiseLike<T>) => void;
    reject: (reason?: any) => void;
}

export function makeDefer<T = any>(timeout?: number, timeoutMessage?: string, disableRejectOnTimeout?: boolean) {
    const o: IDeferred<T> = {
        timeoutMessage,
        promise: null,
        fulfill: null,
        reject: null
    };
    o.promise = new Promise<T>((fullfil, reject) => {
        o.fulfill = fullfil;
        o.reject = reject;
        if (timeout) {
            o.expires = Date.now() + timeout;
            if (!disableRejectOnTimeout) {
                o.timer = setTimeout(reject, timeout, new DefferTimeout(timeoutMessage));
                if (!BROWSER) {
                    o.timer['unref']();
                }
            }
        }
    });
    return o;
}

export class DeferredMap<T = any>{
    protected map: Record<string | number, IDeferred<T>> = Object.create(null);
    constructor(checkInterval?: number) {
        this.checkInterval = checkInterval;
    }
    protected _interval: any;
    protected _checkInterval = 0;
    get checkInterval() {
        return this._checkInterval;
    }
    set checkInterval(v: number) {
        v |= 0;
        if (v !== this._checkInterval) {
            if (!v && !this.empty) {
                throw new DefferError("Can't set 'checkInterval' to 0 when pending promises exists");
            }
            this._checkInterval = v;
            clearInterval(this._interval);
            if (v) {
                this._interval = setInterval(() => {
                    const now = Date.now();
                    for (const id in this.map) {
                        const d = this.map[id];
                        if (d.expires >= now) {
                            this.reject(id, new DefferTimeout(d.timeoutMessage));
                        }
                    }
                }, v);
                if (!BROWSER) {
                    this._interval['unref']();
                }
            }
        }
    }
    get empty() {
        for (const _ in this.map) {
            return false;
        }
        return true;
    }
    keys() {
        return Object.keys(this.map);
    }
    make(id: string | number, timeoutMessage?: string, timeout?: number) {
        timeout ||= this._checkInterval;
        const v = (this.map[id] ||= makeDefer<T>(timeout, timeoutMessage, !!this._checkInterval));
        if (timeout) {
            v.expires = Date.now() + timeout;
        }
        return v.promise;
    }
    has(id: string | number) {
        return this.map[id] != null;
    }
    fulfill(id: string | number, v: T) {
        if (this.map[id]) {
            clearTimeout(this.map[id].timer);
            this.map[id].fulfill(v);
            delete this.map[id];
        }
    }
    reject(id: string | number, err: any) {
        if (this.map[id]) {
            clearTimeout(this.map[id].timer);
            this.map[id].reject(err);
            delete this.map[id];
        }
    }
    rejectAll(err: any) {
        for (const id in this.map) {
            this.reject(id, err);
        }
    }
    rndId() {
        let id = 0;
        do {
            id = rndId();
        } while (this.map[id]);
        return id;
    }
}