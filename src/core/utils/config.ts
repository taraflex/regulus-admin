import { sync } from 'fast-glob';

import { emptyFile } from '@utils/is-empty-file';

const regulus = __non_webpack_require__(__dirname + '/../package.json').regulus || {};

export const IS_LOCAL = process.argv.includes('--local');
export const DEBUG = IS_LOCAL || typeof v8debug === 'object' || /--debug|--inspect/.test(process.execArgv.join(' '));

export const REMOTE_HOSTNAME: string = regulus.hostname;
if (!REMOTE_HOSTNAME) {
    throw 'regulus.hostname required in package.json';
}

export const PEM = IS_LOCAL && sync('*.pem', {
    cwd: __dirname + '/../',
    absolute: true,
    extglob: false,
    braceExpansion: false,
    caseSensitiveMatch: false,
    baseNameMatch: true,
    globstar: false
})[0] || '';
if (IS_LOCAL && emptyFile(PEM)) {
    throw '*.pem sertificate required in project root';
}

export const LOCAL_HOSTNAME: string = IS_LOCAL ? 'dev.' + REMOTE_HOSTNAME : REMOTE_HOSTNAME;
export const PORT = IS_LOCAL ? (regulus.devport | 0) || 7813 : (regulus.port | 0) || 7812;

const routes = Object.assign({
    admin: '/adm',
    api: '/api',
    health: '/health'
}, regulus.routes);

export const ADMIN: string = routes.admin;
export const API: string = routes.api;
export const HEALTH: string = routes.health;
export const STATIC: string = '/static/';