import loadjs from 'loadjs';

export const SRC: string = __webpack_require__.p = document.currentScript?.['src'].substring(0, document.currentScript['src'].lastIndexOf("/") + 1) || __webpack_require__.p;

export default function <T>(entry: string): PromiseLike<T> {
    return loadjs(Object.values(LIB_URLS[entry]).map(u => SRC + u), { returnPromise: true }).then(() => window[entry + 'Lib']);
}