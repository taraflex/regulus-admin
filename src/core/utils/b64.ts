//todo use modern base64url 

const B64_FIX_MAP = { '/': '_', '+': '-', '=': '' } as const;

export function toUrlSafe(s: string | Buffer) {
    const buf = Buffer.isBuffer(s) ? s : Buffer.from(s);
    return buf.toString('base64').replace(/[/+=]/g, s => B64_FIX_MAP[s]);
}

export function fromUrlSafe(s: string) {
    return Buffer.from(s.replace(/[_-]/g, s => s === '-' ? '+' : '/'), 'base64');
}