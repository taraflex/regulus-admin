import { statSync } from 'fs';

export function notEmptyFile(filename: string) {
    try {
        return statSync(filename).size > 0;
    } catch { }
    return false;
}

export function emptyFile(filename: string) {
    return !notEmptyFile(filename);
}