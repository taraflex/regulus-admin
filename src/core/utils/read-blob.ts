export default function (blob: Blob) {
    return new Promise<ArrayBuffer>((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = () => {
            if (reader.error) {
                reject(reader.error);
            } else {
                resolve(reader.result as ArrayBuffer)
            }
        }
        reader.readAsArrayBuffer(blob);
    });
}