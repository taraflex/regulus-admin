import { decode } from '@utils/msgpack';

const utf8decoder = new TextDecoder('utf-8');

export default (v: ArrayBuffer | string, contentType: string) => new Promise(resolve => {
    if (v instanceof ArrayBuffer) {
        try {
            if (contentType?.includes('msgpack')) {
                return resolve(decode(v));
            }
        } catch { }
        try {
            v = utf8decoder.decode(v);
        } catch { }
    }
    if (typeof v === 'string') {
        try {
            return resolve(JSON.parse(v));
        } catch { }
    }
    return resolve(v);

});