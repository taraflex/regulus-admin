import dropHidden from '@utils/drop-hidden-chars';

/*
ЙО, ИО, ЙЕ, ИЕ → И
О, Ы, Я → А
Е, Ё, Э → И
Ю → У
*/
const lgroups = [
    'а - a' + 'я - a' + 'о - o0 ѳө' + 'ы - y',
    'б - b ',
    'в - vb ѵ',
    'г - g ԍ',
    'д - d ԁ',
    'ж - zj ',
    'з - z3 ӡ',
    'и - i ӏіĭ' + 'й - j ѝӣ' + 'е - e ҽ' + 'ё - o' + 'э - e',
    'к - k',
    'л - l ᴫ',
    'м - m ',
    'н - nh',
    'п - pn',
    'р - rp',
    'с - sc',
    'т - t',
    'у - uy' + 'ю - u',
    'ф - f',
    'х - hx',
    'ц - ct',
    'ч - c',
    'шщ - s',
    //'ъь - b ꙏ',
    'q - кԛ',
    'h - һ',
    'w - вԝѡ',
    'j - ј',
    'x - к'
]

const LETTERS = Array.from(new Set(lgroups.join('').replace(/[\-\s]+/g, ''))).join('');

const LR: Record<string, string> = Object.create(null);
for (const l of LETTERS) {
    LR[l] = Array.from(new Set(lgroups.filter(g => g.includes(l)).join('').replace(/[\-\s]+/g, ''))).join('');
}
LR[' '] = ' ';

const NOT_LETTERS = new RegExp('[^' + LETTERS + ']+', 'ugis');

export function extractLetters(s: string) {
    NOT_LETTERS.lastIndex = 0;
    return s ? dropHidden(s).replace(NOT_LETTERS, ' ').trim().toLowerCase() : '';
}

export function toSearchRe(s: string, dist?: number) {
    dist ||= 2;
    s = extractLetters(s);
    const a = [].concat(...Array.from(s, l => LR[l])
        .filter((l, i, a) => a[i - 1] != l)
        .map((l, i, a) => l === ' ' ? [] : [`[^${l}]{${!a[i - 1] || a[i - 1] === ' ' ? '1,' + (dist * 2) : '0,' + dist}}`, `[${l}]+`]));
    return a.length > 3 ? new RegExp(a.slice(1).join(''), 'uis') : null;
}