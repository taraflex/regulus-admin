const escDiv = document.createElement('div');
escDiv.hidden = true;

export default function (html: string) {
    try {
        escDiv.appendChild(document.createTextNode(html));
        return escDiv.innerHTML;
    } finally {
        while (escDiv.lastChild) {
            escDiv.removeChild(escDiv.lastChild);
        }
    }
}