import Router from '@koa/router';
import { noCase } from 'no-case';

type RouteParams = { path?: string | (() => string), name?: string };

export function route(method: string, params?: RouteParams, ...middlewares: Function[]) {
    return (target: { constructor: Function }, prop: string): void => {
        (target.constructor['__routes_info__'] ||= []).push({
            method,
            name: params?.name,
            path: params?.path || '/' + noCase(prop, { delimiter: '-' }),
            prop,
            middlewares
        });
    };
}

export function applyRoutes(router: Router, routes: { constructor: Function }) {
    for (let { method, name, path, prop, middlewares } of routes.constructor['__routes_info__']) {
        if (typeof path === 'function') {
            path = path.call(routes);
        }
        if (name) {
            router[method](name, path, ...middlewares, routes[prop].bind(routes));
        } else {
            router[method](path, ...middlewares, routes[prop].bind(routes));
        }
    }
    return router;
}

export function toRouter(routes: { constructor: Function }) {
    return applyRoutes(new Router(), routes);
}

type DecType = (params?: RouteParams, ...middlewares: Function[]) => (target: { constructor: Function }, prop: string) => void

export const get: DecType = route.bind(null, 'get');
export const head: DecType = route.bind(null, 'head');
export const post: DecType = route.bind(null, 'post');
export const put: DecType = route.bind(null, 'put');
export const del: DecType = route.bind(null, 'delete');
export const patch: DecType = route.bind(null, 'patch');
export const all: DecType = route.bind(null, 'all');