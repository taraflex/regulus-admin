import { Notification } from 'element-ui';

import { stringify } from '@taraflex/string-tools';
import { escHtml } from '@utils/tg-utils';

import type { ElNotificationComponent } from 'element-ui/types/notification';

const nts: Record<string, { w: ElNotificationComponent, count: number }> = Object.create(null);

export default function notify(err: any, level?: FlashLevel) {
    if (err && typeof (err.forEach) === 'function') {
        err.forEach((e: any) => notify(e, level));
    } else {
        const message = stringify(err);
        switch (level || FlashLevel.ERROR) {
            case FlashLevel.ERROR:
                LOG_ERROR(err);
                let { count, w } = nts[message] || {};
                w?.close();
                count = (count | 0) + 1;
                nts[message] = {
                    w: Notification.error({
                        onClose() {
                            delete nts[message];
                        },
                        duration: 0,
                        dangerouslyUseHTMLString: count > 1,
                        message: count > 1 ? `<span><b style="color:#F56C6C;">(${count})</b> ${escHtml(message)}</span>` : message,
                        position: 'top-right',
                        offset: -8
                    } as any),
                    count
                };
                break;
            case FlashLevel.WARNING:
                LOG_WARN(err);
                Notification.warning({
                    message,
                    position: 'top-right',
                    offset: -8
                } as any);
                break;
            default:
                LOG_INFO(err);
                Notification.info({
                    message,
                    position: 'top-right',
                    offset: -8
                } as any);
                break;
        }
    }
}