export default function <Args>(callback: (...args: Args[]) => unknown, time?: number) {
    let t: any;
    return Object.assign((...args: Args[]) => {
        clearTimeout(t);
        t = setTimeout(callback, time || 700, ...args)
    }, {
        dispose() {
            clearTimeout(t);
        }
    });
}
