import { ExtendableError } from '@utils/extendable-error';

class InvalidRegExpError extends ExtendableError { }

export const REGEXP_SYNTAX = /(^\s*\/)(.+)(\/)([mgiyus]{0,6}\s*$)/s;

export default function (s: string) {
    try {
        const [, source, , flags] = s.trim().match(REGEXP_SYNTAX);
        return new RegExp(source, flags || '');
    } catch {
        return new InvalidRegExpError(s);
    }
}