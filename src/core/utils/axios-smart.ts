import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import data2obj from '@utils/data-to-obj';
import rndId from '@utils/rnd-id';
import { cookieStorage } from '@utils/storage';

export const TAB_ID = rndId();
export const BROWSER_ID = cookieStorage.read('x-browser') || cookieStorage.write('x-browser', rndId());

export const baseConfig: AxiosRequestConfig = Object.freeze({
    baseURL: location.origin,
    headers: {
        'x-requested-with': 'XMLHttpRequest',
        'x-tab': TAB_ID,
        'x-browser': BROWSER_ID
    },
    withCredentials: true,
    responseType: 'json',
    validateStatus(_) {
        return true;
    }
});

export class Http<T = any> {
    readonly http: AxiosInstance;
    constructor(config?: AxiosRequestConfig) {
        this.http = Axios.create({
            ...baseConfig,
            ...config
        })
    }
    async get(url: string) {
        const r = await this.http.get<T>(url);
        if (r.status !== 200 && r.status !== 204 && r.status !== 304) {
            throw await data2obj(<any>r.data, r.headers['content-type']);
        }
        return r;
    }
    async put(url: string, payload?) {
        const r = await this.http.put<T>(url, payload);
        if (r.status !== 201 && r.status !== 204) {
            throw await data2obj(<any>r.data, r.headers['content-type']);
        }
        return r;
    }
    async patch(url: string, payload?) {
        const r = await this.http.patch<T>(url, payload);
        if (r.status !== 201) {
            throw await data2obj(<any>r.data, r.headers['content-type']);
        }
        return r;
    }
    async delete(url: string) {
        const r = await this.http.delete(url);
        if (r.status !== 204) {
            throw await data2obj(r.data, r.headers['content-type']);
        }
        return r;
    }
}

export const httpJson = new Http();
export const httpMsgpack = new Http<ArrayBuffer>({
    responseType: 'arraybuffer',
    headers: { ...baseConfig.headers, 'Content-Type': 'application/x-msgpack' }
});