export default function <T>(a: T, ...props: (keyof T)[]) {
    a = { ...a };
    for (const p of props) {
        delete a[p];
    }
    return a;
}