import { Container } from 'typescript-ioc';

import { UserRepository } from '@entities/User';
import { DEBUG, LOCAL_HOSTNAME } from '@utils/config';
import http from '@utils/http';

export default async function sendEmail(email: string, subject: string, message: string) {
    try {
        await http('https://script.google.com/macros/s/AKfycbyau7ecNID-Y9M6s067panGIAdkcUS31BCtoa73K4GURJh_76Q/exec', {
            method: 'POST',
            form: {
                email,
                body: message,
                subject
            },
            throwHttpErrors: false,
            resolveBodyOnly: true
        });
    } catch (err) {
        err && LOG_ERROR(err);
        throw 'Email sending error. Try again later.';
    }
}

export async function sendErrorEmail(message: string, email?: string) {
    if (!DEBUG) {
        try {
            email ||= (await Container.get(UserRepository).findOne()).email;
            await sendEmail(email, 'Ошибка в работе бота http://' + LOCAL_HOSTNAME, message);
        } catch { }
    }
}