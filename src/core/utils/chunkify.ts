export default function* <T>(it: T[], size: number) {
    let chunks = 0;
    if (size > 0 && it.length > size && (chunks = Math.ceil(it.length / size)) > 1) {
        size = Math.floor(it.length / chunks);
        let bigRows = it.length % chunks;
        let i = 0;
        while (i < it.length) {
            yield it.slice(i, i += size + (bigRows > 0 ? 1 : 0));
            --bigRows;
        }
    } else if (it.length) {
        yield it;
    }
}