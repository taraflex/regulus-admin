import marked from 'marked';
import { sentenceCase } from 'sentence-case';
import { compile } from 'vue-template-compiler';

import { findPrimaryField, has } from '@crud/helpers';
import single from '@templates/components/single.pug';
import table from '@templates/components/table.pug';
import type { Can, RTTI } from '@crud';
import { ValidationRuleObject } from '@crud/rules';

const templates = { single, table };

const renderer = new marked.Renderer({ xhtml: true });
const linkRenderer = renderer.link;
renderer.link = (href, title, text) => linkRenderer.call(renderer, href, title, text).replace(/^<a /, '<a target="_blank" ');

function toFunction(code: string) {
    return `(function(){${code}})`;
}

export default ({ props, display }: RTTI, can: Record<Can, boolean>) => {
    const vueTemplate = templates[display.type]({
        props,
        EDIT: RowState.EDIT,
        NONE: RowState.NONE,
        REMOVE: RowState.REMOVE,
        SAVE: RowState.SAVE,
        ASC: SortType.ASC,
        DESC: SortType.DESC,
        can,
        findPrimaryField,
        isVisibleField(v: ValidationRuleObject) {
            return !has(v, RTTIItemState.HIDDEN)
        },
        isEditableField(v: ValidationRuleObject) {
            return !has(v, RTTIItemState.READONLY)
        },
        propTitle(v: ValidationRuleObject, field: string) {
            return v.description ? marked.parseInline(v.description, { xhtml: true, renderer }) : sentenceCase(field);
        }
    });

    const { render, staticRenderFns } = compile(vueTemplate, { preserveWhitespace: false });
    return `{render(){${render}},staticRenderFns:[${staticRenderFns.map(toFunction).join(',')}]}`;
}