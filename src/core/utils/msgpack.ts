import {
    createCodec, decode as dec, DecoderOptions, encode as enc, EncoderOptions
} from 'msgpack-lite';

const opts = {
    codec: createCodec({
        preset: true,
        safe: false,
        useraw: false,
        int64: false,
        binarraybuffer: true,
        uint8array: BROWSER,
        usemap: false
    })
} as EncoderOptions | DecoderOptions;

opts.codec.addExtPacker(0x3F, URL, u => encode(u.href));
opts.codec.addExtUnpacker(0x3F, data => new URL(decodeBuffer(data)));

export function decode(data: ArrayBuffer) {
    return dec(new Uint8Array(data, 0), opts);
}

export function decodeBuffer(data: Uint8Array) {
    return dec(data, opts);
}

export function encode(o: any) {
    return enc(o, opts);
}