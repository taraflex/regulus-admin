import escapeRe from 'escape-string-regexp';

import rndId from '@utils/rnd-id';

export function toChatId(supergroup_id: number) {
    return +('-100' + supergroup_id);
}

export function toSupergroupId(chat_id: number) {
    return +chat_id.toString().replace(/^-100/, '');
}

export function getCommandParser(cmd: string) {
    const re = new RegExp('^\\/' + cmd + '(\\s+|$)', 'i');
    return (text: string): string => re.test(text) ? text.substr(cmd.length + 2).trim() : null;
}

export const startCmdPayload = getCommandParser('start');

export function invisibleLink(url?: string) {
    return `[\u00A0](${url || rndId().toFixed()})⁠`;
}

export function map2replacer(map: Record<string, string>) {
    const re = new RegExp(Object.keys(map).map(escapeRe).join('|'), 'g');
    return (s: string) => s ? s.replace(re, m => map[m]) : '';
}

const tagged = {
    '$': '＄',
    '`': '`',
    '\\': '⧵',
    '\u2028': '\n',
    '\u2029': '\n'
};

export const escTaggedString = map2replacer(tagged);

export const escHtml = map2replacer({
    ...tagged,
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
});

export const escTgMarkdownSameLength = map2replacer({
    ...tagged,
    '*': '✶',
    '#': '＃',
    '[': '［',
    ']': '］',
    '_': '＿',
    '~': '∼',
    '@': '＠',
    '/': '∕',
});

export const escTgMarkdown = map2replacer({
    ...tagged,
    '~': '\u200D~\u200D',
    '*': '\u200D*\u200D',
    '](': ']\u200D(',
    '_': '\u200D_\u200D',
    '@': '@\u200D',
    '#': '#\u200D',
    '/': '/\u200D'
});

export const BOT_TOKEN_RE = /^\d{9,}:[\w-]{35}$/;