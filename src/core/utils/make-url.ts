import { ExtendableError } from '@utils/extendable-error';

class InvalidURLError extends ExtendableError { }

export default function (s: string) {
    try {
        return new URL(s);
    } catch {
        return new InvalidURLError(s);
    }
}