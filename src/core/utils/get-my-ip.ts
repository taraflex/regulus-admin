import { isIP } from 'net';

import http from '@utils/http';

export default async function () {
    let ip = '';
    try {
        ip = (await http('http://ipecho.net/plain').text()).trim();
        if (!isIP(ip)) {
            throw null;
        }
    } catch {
        try {
            ip = (await http('http://ident.me').text()).trim();
            if (!isIP(ip)) {
                throw null;
            }
        } catch {
            ip = (await http('http://icanhazip.com').text()).trim();
            if (!isIP(ip)) {
                throw 'Invalid ip: ' + ip;
            }
        }
    }
    return ip;
}