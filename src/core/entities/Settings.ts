import { PrimaryGeneratedColumn, Repository } from 'typeorm';

import { seedRndString } from '@utils/rnd';
import { v } from '@crud';

export abstract class Settings {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ default: seedRndString() })
    secret: string;

    @v()
    installed: boolean;
}

export abstract class SettingsRepository extends Repository<Settings>{ }