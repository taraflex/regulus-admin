import { Column } from 'typeorm';

export class File {
    @Column({ default: '' })
    filename: string;

    @Column({ default: '' })
    path: string;

    ready?: boolean;
}