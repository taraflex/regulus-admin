import { Column } from 'typeorm';

import { typeDefault, URL_TRANSFORMER } from '@crud/helpers';

export class Link {
    @Column({ default: '' })
    title: string;

    @Column({ type: 'text', default: URL_TRANSFORMER.to(typeDefault({ type: 'realurl' })), transformer: URL_TRANSFORMER })
    url: URL;
}