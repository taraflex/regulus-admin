import { PrimaryGeneratedColumn, Repository } from 'typeorm';

import { v } from '@crud';

declare module 'koa' {
    interface Context {
        state: { user?: User }
    }
}

export abstract class User {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /\S+@\S+/, uniqueCol: true })
    email: string;

    @v({ min: 6, max: 128 })
    password: string;

    @v({ integer: true, default: 0b1111111111111111111111111111111 })
    role: number;
}

export abstract class UserRepository extends Repository<User>{ }