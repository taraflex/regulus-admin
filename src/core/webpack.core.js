'use strict';

const { builtinModules } = require('module');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { IgnorePlugin } = require('webpack');
const { resolve } = require('path');
const { tosource } = require('@taraflex/string-tools');
const AssetsPlugin = require('assets-webpack-plugin');
const config = require('./webpack.base');
const fs = require('fs');
const merge = require('merge-deep');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const VirtualModulesPlugin = require('webpack-virtual-modules');

function genMonacoTypes() {
    //@ts-ignore
    const mrequire = require('esm')(module);
    const { libFileMap } = mrequire('monaco-editor/esm/vs/language/typescript/lib/lib.js');
    for (let k in libFileMap) {
        if (k.includes('.esnext.') || k.includes('.dom.') || k.includes('.webworker.') || k.includes('.scripthost.')) {
            delete libFileMap[k];
        } else {
            libFileMap[k] = libFileMap[k].slice(810);
        }
    }
    libFileMap['lib.node.d.ts'] = fs.readFileSync(__dirname + '/frontend/monaco/node.d.ts.txt', 'utf-8').replace(
        '/* tslint:disable-next-line:callable-types */',
        Object.keys(require('../../package.json').dependencies)
            .concat(builtinModules)
            .map(m => `(id: "${m}"): typeof import("${m}");`)
            .join('\n')
    );
    return libFileMap;
}

const hasVendor = !process.argv.some(s => /no-?vendor/i.test(s));
const useSmp = process.argv.some(s => /measure/i.test(s));

let smp = null;
function smp_wrap(o) {
    return useSmp ? (smp || (smp = new SpeedMeasurePlugin())).wrap(o) : o;
}

function initAssest(name, processOutput, fileTypes = ['js', 'css']) {
    return new AssetsPlugin({
        fullPath: false,
        includeAllFileTypes: false,
        fileTypes,
        filename: name + '-assets.json',
        path: resolve(__dirname, '../../build'),
        entrypoints: true,
        processOutput
    })
}

function makeAnalyzer(prefix) {
    return new BundleAnalyzerPlugin({
        logLevel: 'warn',
        analyzerMode: 'static',
        openAnalyzer: false,
        reportFilename: prefix + '-report.htm',
        excludeAssets: /common\.js[^\/\\]*$/i
    })
}

module.exports = function (webConfig, nodeConfig) {
    return config.u(
        smp_wrap(merge(
            config(false),
            {
                name: 'node',
                entry: {
                    index: './src/core/index.ts'
                },
                plugins: config.DEV ? [] : [makeAnalyzer('node')]
            },
            nodeConfig || {}
        )),
        smp_wrap(merge(
            config(true, true),
            {
                name: 'frontend',
                entry: {
                    index: './src/core/frontend/index.ts'
                },
                externals: {
                    'monaco-editor': 'monacoLib',
                    'xterm': 'xtermLib'
                },
                module: {
                    noParse: /(monaco-editor|xterm).*(?<!\.css)$/i
                },
                plugins: config.u(
                    initAssest('frontend'),
                    !config.DEV && makeAnalyzer('../frontend')
                )
            },
            webConfig || {}
        )),
        hasVendor && merge(
            config(true),
            {
                name: 'libs',
                entry: {
                    common: './src/core/frontend/common.css',
                    xterm: './src/core/frontend/terminal/lib.ts',
                    monaco: './src/core/frontend/monaco/lib.ts'
                },
                output: {
                    library: '[name]Lib',
                    libraryTarget: 'window'
                },
                plugins: config.u(
                    new IgnorePlugin({
                        resourceRegExp: /^\.\/(?!javascript|typescript)[\w-]+\.js$/,
                        contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]basic-languages[\/\\]/
                    }),
                    new IgnorePlugin({
                        resourceRegExp: /^\.\/(cssMode|htmlMode|jsonMode)\.js$/,
                        contextRegExp: /monaco-editor[\/\\]esm[\/\\]vs[\/\\]language[\/\\]/
                    }),
                    new CleanWebpackPlugin({
                        cleanStaleWebpackAssets: false,
                        cleanOnceBeforeBuildPatterns: [],
                        cleanAfterEveryBuildPatterns: ['common.js*'],
                        verbose: false
                    }),
                    initAssest('common', function (assets) {
                        delete assets.monaco;
                        delete assets.xterm;
                        return JSON.stringify(assets)
                    }, ['css']),
                    initAssest('libs'),
                    !config.DEV && makeAnalyzer('../libs')
                )
            }),
        hasVendor && merge(
            config(true),
            {
                name: 'workers',
                target: 'webworker',
                entry: {
                    'editor.worker': 'monaco-editor/esm/vs/editor/editor.worker.js',
                    'typescript.worker': 'monaco-editor/esm/vs/language/typescript/ts.worker'
                },
                plugins: config.u(
                    new VirtualModulesPlugin([genMonacoTypes()].map(t => ({
                        'node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.js': 'export var libFileMap = ' + tosource(t),
                        'node_modules/monaco-editor/esm/vs/language/typescript/lib/lib.index.js': 'export var libFileSet = {' + Object.keys(t).map(k => `'${k}':true`).join(',') + '}'
                    }))[0]),
                    initAssest('workers'),
                    !config.DEV && makeAnalyzer('../workers')
                )
            })
    );
}