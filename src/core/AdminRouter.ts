import { AuthRegister } from 'core/AuthRegister';
import { createDBFilename, DBConnection } from 'core/DBConnection';
import { MainRouter } from 'core/MainRouter';
import { AuthRoutes } from 'core/routes/auth';
import { CheckPassRoutes } from 'core/routes/checkpass';
import { InstallRoutes } from 'core/routes/install';
import { ResetEmailRoutes } from 'core/routes/resetemail';
import { ResetPassRoutes } from 'core/routes/resetpass';
import { createReadStream } from 'fs';
import cp from 'fs-cp';
import compress from 'koa-compress';
import { noCase } from 'no-case';
import { parse as pathToRegexp } from 'path-to-regexp';
import plur from 'plur';
import { Container, Inject, Singleton } from 'typescript-ioc';

import { has } from '@crud/helpers';
import { ValidatorType } from '@crud/rules';
import { Settings } from '@entities/Settings';
import { entities } from '@generated/entities';
import Router from '@koa/router';
import checkAuth from '@middlewares/check-auth';
import flash from '@middlewares/flash';
import installed from '@middlewares/installed';
import redirectAfterError from '@middlewares/redirect-after-error';
import removeTralingSlashes from '@middlewares/remove-traling-slashes';
import { isValidVar, tosource } from '@taraflex/string-tools';
import componentsTemplate from '@templates/components/index.mustache';
import dataPath from '@utils/data-path';
import { applyRoutes } from '@utils/routes-helpers';
import rtti2vue from '@utils/rtti-to-vue-component';

import type { Context } from 'koa';
import type { Hook } from '@crud';

function restart(ctx: Context) {
    ctx.type = 'json';
    ctx.body = '{"success":"ok"}';
    ctx.res.once('finish', () => process.exit(2));
}

@Singleton
export class AdminRouter extends Router<any, any> {

    constructor(
        @Inject settings: Settings,
        @Inject dbConnection: DBConnection,
        @Inject installRoutes: InstallRoutes,
        @Inject authRoutes: AuthRoutes,
        @Inject resetPassRoutes: ResetPassRoutes,
        @Inject checkPassRoutes: CheckPassRoutes,
        @Inject resetEmailRoutes: ResetEmailRoutes,
        @Inject { session, passport, passportSession }: AuthRegister
    ) {
        super();

        this.use(
            flash,
            removeTralingSlashes,
            redirectAfterError('login'),
            session,
            passport,
            passportSession
        );

        applyRoutes(this, installRoutes);
        if (!settings.installed) {
            this.use(installed(settings));
        }
        applyRoutes(this, authRoutes);
        applyRoutes(this, resetPassRoutes);

        this.use(checkAuth);
        applyRoutes(this, checkPassRoutes);
        applyRoutes(this, resetEmailRoutes);

        const componentsCache = {};
        const pureSearchableTypes = new Set<ValidatorType>([
            'javascript',
            'string',
            'wysiwyg',
            'md-tg',
            'array',
            'enum'
        ])

        this.get('root', '/', (ctx: Context) => {
            const user = ctx.state.user;
            if (!componentsCache[user.role]) {
                const components = [];
                for (const entity of entities) {
                    let { name, rtti, checkAccess, hooks } = entity;

                    const can = Object.create(null);
                    for (const hook of ['GET', 'PATCH', 'PUT', 'DELETE'] as Hook[]) {
                        try {
                            can[hook] = checkAccess(hook, ctx.state.user);
                        } catch { }
                    }
                    if (can.GET) {
                        can.CHANGE = can.PUT || can.PATCH;
                        name = plur(name, 2);//todo fix sesstingses

                        components.push({
                            ...rtti.display,
                            name,
                            render: rtti2vue(rtti, can),
                            can: tosource(can, null, ''),
                            rtti: tosource(rtti, null, ''),
                            hooks: tosource(hooks, null, ''),
                            slug: noCase(name, { delimiter: '-' }),
                            remote: Object.values(rtti.props)
                                .filter(v => v.remote)
                                .map(v => {
                                    const i = v.remote.lastIndexOf('.');
                                    const c = v.remote.slice(0, i);
                                    const method = v.remote.slice(i + 1);
                                    if (!isValidVar(c) || !isValidVar(method)) {
                                        throw `Invalid property ${tosource(v)}`;
                                    }
                                    return { c, method };
                                }),
                            searchable: Object.entries(rtti.props).map(([prop, info]) => {
                                if (has(info, RTTIItemState.HIDDEN)) return '';
                                if (pureSearchableTypes.has(info.type)) return `String(x.${prop}||'')`;
                                if (info.type === 'link') return `((x.${prop}||{}).title||'')`;
                                if (info.type === 'file') return `(x.${prop}||{}).filename`;
                                //if (info.type === 'files') return `(x.${prop}||[]).map(f=>f.filename).join(' ')`;
                            }).filter(Boolean).join('+" "+')
                        });
                    }
                }

                const { stack } = Container.get(MainRouter);
                componentsCache[user.role] = componentsTemplate({
                    components,
                    Paths: stack
                        .filter(l => l.name)
                        .map(l => {
                            if (l.paramNames.length > 0) {
                                const tokens = pathToRegexp(l.path);
                                return {
                                    name: l.name,
                                    url: `(${l.paramNames.map(p => p.name).join(', ')}) => ` + tokens.map(p => p.constructor === String ? JSON.stringify(p) : (p as any).name).join(' + "/" + ')
                                }
                            } else {
                                return {
                                    name: l.name,
                                    url: JSON.stringify(l.path)
                                }
                            }
                        }),
                    LIB_URLS: tosource(__non_webpack_require__(__dirname + '/libs-assets.json')),
                    MONACO_WORKER_URLS: tosource(__non_webpack_require__(__dirname + '/workers-assets.json'))
                });
            }

            ctx.pug('index', {
                entries: ['frontend'],
                inlineScript: `window.UserInfo = {email:${tosource(user.email)}};window.Flash = ${ctx.flash ? tosource(ctx.flash) : '{}'};` + componentsCache[user.role]
            });
        });

        this.get('backup', '/backup', compress(), (ctx: Context) => {
            ctx.noStore();
            ctx.attachment(createDBFilename());
            ctx.body = createReadStream(dbConnection.filename);
        });

        //put вместо get/post потому что возможен csrf
        this.put('restore', '/restore', async (ctx: Context) => {
            await cp(ctx.req, dataPath(createDBFilename()));
            restart(ctx);
        });

        this.put('restart', '/restart', restart);
    }
}