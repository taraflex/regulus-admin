import { AuthRegister } from 'core/AuthRegister';
import { DBConnection } from 'core/DBConnection';
import { noCase } from 'no-case';
import plur from 'plur';
import { Container, Inject, Singleton } from 'typescript-ioc';

import { has } from '@crud/helpers';
import { entities } from '@generated/entities';
import Router from '@koa/router';
import bodyParseMsgpack from '@middlewares/body-parse-msgpack';
import checkEntityAccess from '@middlewares/check-entity-access';
import { encode } from '@utils/msgpack';

import type { Context } from 'koa';
import type { ObjectLiteral, Repository } from 'typeorm';
import type { RTTI } from '@crud';

export async function data2Model(ctx: Context, model: any, repository: Repository<ObjectLiteral>): Promise<ObjectLiteral> {
    const { body } = ctx.request;
    const { props } = model.constructor.rtti as RTTI;
    for (const k in body) {
        const p = props[k];
        if (p && !has(p, RTTIItemState.READONLY)) {
            model[k] = body[k];
        }
    }
    if (model.beforeFilter) {
        await model.beforeFilter();
    }
    const o = await repository.save(model);
    if (o.afterFilter) {
        await o.afterFilter();
    }
    return o;
}

export function msgpack(ctx: Context, data: any) {
    ctx.type = 'application/x-msgpack';
    ctx.sendNoneMatch(
        Array.isArray(data) ?
            (data.length > 0 ? encode(data) : null) :
            (data != null ? encode(data) : null)
    );
}

@Singleton
export class ApiRouter extends Router {

    constructor(@Inject connection: DBConnection, @Inject { session, passport, passportSession }: AuthRegister) {
        super();
        this.use(session, passport, passportSession);
        for (const e of entities) {
            const repository = connection.getRepository(e);

            const access = checkEntityAccess(e);
            const name = noCase(plur(e.name, 2), { delimiter: '-' });
            const singltonInstance = e.asyncProvider ? Container.get(e) : null;

            const saveModel = singltonInstance ? async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (const p in props) {
                    singltonInstance[p] = o[p];
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            } : async (ctx: Context, model: any) => {
                const o = await data2Model(ctx, model, repository);
                const { props } = e.rtti;
                for (const p in props) {
                    if (!has(props[p], RTTIItemState.SEND_ALWAYS)) {
                        delete o[p];
                    }
                }
                ctx.status = 201;
                msgpack(ctx, o);
            }

            this
                .get(name, '/' + name, access, async (ctx: Context) => {
                    msgpack(ctx, await repository.find());
                })
                .get(`/${name}/:id`, access, async (ctx: Context) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    msgpack(ctx, model);
                })
                .put('/' + name, access, bodyParseMsgpack, (ctx: Context) => saveModel(ctx, new e()))
                .patch(`/${name}/:id`, access, bodyParseMsgpack, async (ctx: Context) => {
                    const model = await repository.findOne(ctx.params.id);
                    if (!model) {
                        throw 404;
                    }
                    await saveModel(ctx, model);
                })
                .delete(`/${name}`, access, async (ctx: Context) => {
                    await repository.clear();
                    ctx.body = null;
                })
                .delete(`/${name}/:id`, access, async (ctx: Context) => {
                    await repository.delete(ctx.params.id);
                    ctx.body = null;
                });
        }
    }
}