import 'core/preload';

import { App } from 'App';
import { AdminRouter } from 'core/AdminRouter';
import { ApiRouter } from 'core/ApiRouter';
import { AuthRegister } from 'core/AuthRegister';
import { dbInit } from 'core/DBConnection';
import { Koa } from 'core/Koa';
import { MainRouter } from 'core/MainRouter';
import { resolve4 } from 'dns';
import { createServer, IncomingMessage, OutgoingHttpHeaders } from 'http';
import { Context, ParameterizedContext } from 'koa';
import mount from 'koa-mount';
import send from 'koa-send';
import { normalize } from 'path';
import { Container, Inject, Singleton } from 'typescript-ioc';
import { promisify } from 'util';
import { Server as WSServer } from 'ws';

import { ServerRPC, startPingClients } from '@rpc/ServerRPC';
import { ADMIN, API, IS_LOCAL, LOCAL_HOSTNAME, PORT, REMOTE_HOSTNAME, STATIC } from '@utils/config';
import getMyIp from '@utils/get-my-ip';


@Singleton
class Main {
    constructor(
        @Inject private readonly baseApp: App,
        @Inject private readonly authRegister: AuthRegister,
        @Inject private readonly app: Koa,
        @Inject private readonly router: MainRouter,
        @Inject private readonly apiRouter: ApiRouter,
        @Inject private readonly adminRouter: AdminRouter
    ) { }

    async main() {
        const { router, apiRouter, adminRouter, app, baseApp } = this;

        await baseApp.init();

        router.use(API, apiRouter.routes(), apiRouter.allowedMethods())
        router.use(ADMIN, adminRouter.routes(), adminRouter.allowedMethods())

        app
            //routes
            .use(router.routes())
            .use(router.allowedMethods())
            //static assets
            .use(mount(STATIC, (ctx: Context) => send(ctx, ctx.path, {
                root: normalize(__dirname + STATIC),
                maxage: 365 * 24 * 60 * 60 * 1000,
                gzip: !IS_LOCAL,
                brotli: false,
                setHeaders: IS_LOCAL ? (res: ParameterizedContext['res']) => {
                    res.setHeader('Access-Control-Allow-Origin', 'http://' + LOCAL_HOSTNAME);
                    res.setHeader('Cache-Control', 'no-store, no-cache, max-age=0');
                } : undefined
            })));

        const server = createServer(app.callback());

        const { passportComposed } = this.authRegister;

        const wss = new WSServer({
            server,
            verifyClient: (
                info: { origin: string; secure: boolean; req: IncomingMessage }
                , callback: (res: boolean, code?: number, message?: string, headers?: OutgoingHttpHeaders) => void
            ) => {
                try {
                    const ctx = app.createContext(info.req, null);
                    passportComposed(ctx, () => (callback(!!ctx.state.user), null)).catch(LOG_ERROR);
                } catch (err) {
                    LOG_ERROR(err);
                }
            }
        });

        wss.on('connection', async (socket, request) => {
            try {
                const ctx = app.createContext(request, null);
                await passportComposed(ctx, () => (new ServerRPC(socket, ctx as Context), null));
            } catch (err) {
                LOG_ERROR(err);
                socket.close();
            }
        });

        startPingClients();

        const [currentIp, hostIps] = await Promise.all([
            await getMyIp(),
            await promisify(resolve4)(LOCAL_HOSTNAME).catch(LOG_ERROR),
            new Promise<void>(resolve => server.listen(PORT, resolve))
        ]);

        console.log(`Current ip: ${currentIp} 
Host: ${REMOTE_HOSTNAME} [${hostIps || ''}]
Admin url: http://${LOCAL_HOSTNAME}` + ADMIN);
    }
}

dbInit()
    .then(() => Container.get(Main).main())
    .catch(err => {
        LOG_ERROR(err);
        process.exit(1);
    });