import Backoff from 'backo2';

import { Answer, Request, RPC } from '@rpc/RPC';
import { BROWSER_ID, TAB_ID } from '@utils/axios-smart';
import { makeDefer } from '@utils/Deferred';
import delay from '@utils/delay';
import { encode } from '@utils/msgpack';
import notify from '@utils/notify';
import syncDebounce from '@utils/sync-debounce';

import type { IDisposable, Terminal } from 'xterm';

export class ClientRPC extends RPC {
    protected term?: Terminal;
    protected ws: WebSocket;
    protected termListeners: IDisposable[] = [];
    protected readonly url = location.origin.replace(/^http/i, 'ws') + '/ws/';
    protected readonly backoff = new Backoff({ min: 1000, max: 60 * 1000 });
    protected chain = makeDefer();
    constructor() {
        super();
        this._restart();
    }
    protected _lockRestart = false;
    protected readonly _restart = async () => {
        if (this._lockRestart) {
            return;
        }
        try {
            this._lockRestart = true;
            this.dispose();
            const d = this.backoff.duration();
            if (d > 1000) {
                await delay(d);
            }
            if (this.term) {
                const { cols, rows } = this.term;
                this.ws = new WebSocket(this.url + `?cols=${cols}&rows=${rows}&x-tab=${TAB_ID}&x-browser=${BROWSER_ID}`);
            } else {
                this.ws = new WebSocket(this.url + `?x-tab=${TAB_ID}&x-browser=${BROWSER_ID}`);
            }
            this.ws.binaryType = 'arraybuffer';
            this.ws.onerror = this.onerror;
            this.ws.onclose = this._restart;
            this.ws.onmessage = this.onmessage;
            this.ws.onopen = this.onopen;
        } finally {
            this._lockRestart = false;
        }
    }
    protected readonly onerror = () => {
        notify(navigator.onLine === false ? 'Connection error. Posible offline.' : 'Connection error');
        this.restart();
    }
    protected readonly onopen = () => {
        this.backoff.reset();
        this.chain.fulfill(true);
        this.chain = makeDefer();
    }
    async enableTerminal(term: Terminal) {
        if (!this.opened) {
            await this.chain.promise;
        }
        const { searchParams } = new URL(this.ws.url);
        const serverRPC = this.remote<RPC>('RPC');
        this.term = term;
        if (
            +searchParams.get('cols') !== term.cols ||
            +searchParams.get('rows') !== term.rows
        ) {
            await serverRPC.enableTerminal({ cols: term.cols, rows: term.rows });
        }
        this.termListeners.forEach(l => l.dispose());
        this.termListeners.length = 0;
        this.termListeners.push(term.onData(data => this.ws.send(data)));
        const onResize = syncDebounce(size => {
            if (this.opened) {
                serverRPC.enableTerminal(size).catch(notify);
            }
        }, 1000);
        this.termListeners.push(term.onResize(onResize));
        this.termListeners.push(onResize);
    }
    protected readonly onmessage = (message: MessageEvent) => {
        this.process(message.data).catch(notify);
    }
    protected async send(data: Request | Answer) {
        if (!this.opened) {
            await this.chain.promise;
        }
        this.ws.send(encode(data));
    }
    get opened() {
        return this.ws?.readyState === WebSocket.OPEN;
    }
    get closed() {
        return !this.ws || this.ws.readyState === WebSocket.CLOSED || this.ws.readyState === WebSocket.CLOSING;
    }
    restart() {
        if (this.closed) {
            this._restart();
        }
    }
    dispose() {
        if (this.ws) {
            this.ws.onclose = undefined;
            this.ws.onerror = undefined;
            this.ws.onopen = undefined;
            this.ws.onmessage = undefined;
            if (!this.closed) {
                this.ws.close();
            }
            this.ws = null;
        }
    }
}