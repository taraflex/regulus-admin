import mem from 'mem';

import { DeferredMap } from '@utils/Deferred';
import { ExtendableError } from '@utils/extendable-error';
import { decode } from '@utils/msgpack';

export type Answer = {
    //error
    e?: any,
    //result
    r?: any,
    //id
    i: number
}

export type Request = {
    //class
    c: string,
    //function
    f: string,
    //arguments
    a: any[],
    //id
    i: number
}

export type ITerminal = {
    write(data: string): void;
}

const CLASSES_FOR_REMOTE_CALL = Object.create(null);
const CALLS_DMAP = new DeferredMap(30000);

class RemoteHandler {
    constructor(
        private readonly klass: string,
        private readonly rpc: RPC
    ) { }
    get(_: any, f: string) {
        return (...a: any[]) => {
            const i = CALLS_DMAP.genId();
            try {
                const promise = CALLS_DMAP.make(i, 'Call [' + this.klass + '.' + f + '] timed out');
                this.rpc['send']({ i, c: this.klass, f, a });//send protected
                return promise;
            } catch (e) {
                CALLS_DMAP.reject(i, e);
                throw e;
            }
        }
    }
}

export class RPCError extends ExtendableError { }

export abstract class RPC {
    protected term?: ITerminal;

    static register(klass: string, o: any) {
        CLASSES_FOR_REMOTE_CALL[klass] = o;
    }

    static unregister(klass: string) {
        delete CLASSES_FOR_REMOTE_CALL[klass];
    }

    readonly remote = mem(<I = any>(key: string): Record<keyof I, (...args: any[]) => Promise<any>> => {
        return new Proxy(Object.create(null), new RemoteHandler(key, this));
    });

    protected async process(message: string | ArrayBuffer) {
        if (message) {
            if (message.constructor === String) {
                this.term && this.term.write(message as string);
            } else {
                const data = decode(message as ArrayBuffer) as Request & Answer;
                if (data.f) {
                    const { i, f, c, a } = data as Request;
                    try {
                        if (c === 'RPC') {
                            if (f === 'enableTerminal') {
                                this.send({
                                    i,
                                    r: await this.enableTerminal(a[0])
                                });
                            } else {
                                throw new RPCError(`Deny method [${f}] called on RPC`);
                            }
                        } else {
                            this.send({
                                i,
                                r: await this.rcall(CLASSES_FOR_REMOTE_CALL[c], f, a)
                            });
                        }
                    } catch (e) {
                        this.send({ i, e });
                    }
                } else if (data.e != null) {
                    CALLS_DMAP.reject(data.i, data.e);
                } else {
                    CALLS_DMAP.fulfill(data.i, data.r);
                }
            }
        }
    }

    protected abstract rcall(o: any, fname: string, args: any[]): any;
    protected abstract send(data: Request | Answer): void;
    abstract enableTerminal(_: any): void | Promise<void>;
    abstract dispose(): void;
}