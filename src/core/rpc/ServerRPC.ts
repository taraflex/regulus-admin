import { IPty, spawn } from 'node-pty';
import ServerWebSoket from 'ws';

import { Answer, Request, RPC } from '@rpc/RPC';
import { IS_LOCAL, PEM, PORT, REMOTE_HOSTNAME } from '@utils/config';
import { encode } from '@utils/msgpack';
import { DEV_NULL } from '@utils/proxy-info';

import type { Context } from 'koa';

const tmuxArgs = ['new', '-A', '-s', APP_NAME + '-' + PORT];

const SESSIONS = new Map<number, Map<number, ServerRPC>>();

function tabId(ctx: Context) {
    return parseInt(ctx.headers['x-tab'] as string) ||
        parseInt(ctx.query['x-tab'] as string) ||
        0;
}

function browserId(ctx: Context) {
    return parseInt(ctx.getCookie('x-browser')) ||
        parseInt(ctx.headers['x-browser'] as string) ||
        parseInt(ctx.query['x-browser'] as string) ||
        0;
}

export function startPingClients() {
    setInterval(() => SESSIONS.forEach(m => m.forEach(rpc => rpc.ping(10000))), 30000)['unref']();
}

export function fromCtx(ctx: Context): ServerRPC {
    return SESSIONS.get(browserId(ctx)).get(tabId(ctx));
}

export function destroyBrowserConnections(ctx: Context) {
    const b = SESSIONS.get(browserId(ctx));
    if (b) {
        b.forEach(v => v.dispose());
        b.clear();
    }
}

export function destroyAllConnections() {
    for (const b of SESSIONS.values()) {
        if (b) {
            b.forEach(v => v.dispose());
            b.clear();
        }
    }
    SESSIONS.clear();
}

const dataSendCb = (err: any) => err && console.error('(__FILE__)', err);

export class ServerRPC extends RPC {
    protected lastActive: number = Date.now();
    protected term?: IPty;

    private readonly onWSMessage = (message: string | ArrayBuffer) => {
        this.lastActive = Date.now();
        this.process(message).catch(LOG_ERROR);
    }

    private readonly onWSClose = () => {
        try {
            this.dispose();
        } catch (err) {
            LOG_ERROR(err);
        }
    }

    private readonly onWSPong = () => {
        this.lastActive = Date.now();
    }

    protected readonly browserId: number;
    protected readonly tabId: number;

    constructor(
        protected readonly socket: ServerWebSoket,
        ctx: Context
    ) {
        super();

        this.browserId = browserId(ctx);
        this.tabId = tabId(ctx);
        if (!this.tabId) {
            throw 'Invalid websocket tab';
        }

        let tabs = SESSIONS.get(this.browserId);
        if (!tabs) {
            SESSIONS.set(this.browserId, tabs = new Map());
        }
        tabs.set(this.tabId, this);

        socket.on('close', this.onWSClose);
        socket.on('pong', this.onWSPong);
        socket.on('message', this.onWSMessage);

        if (ctx.query.cols && ctx.query.rows) {
            this.enableTerminal({
                cols: +ctx.query.cols || 100,
                rows: +ctx.query.rows || 40
            }).catch(LOG_ERROR);
        }
    }

    protected send(data: Request | Answer) {
        return new Promise<void>((fulfill, reject) => {
            const r = SESSIONS.get(this.browserId)?.get(this.tabId);
            if (r !== this) {
                this.dispose();
            }
            if (r) {
                r.socket.send(encode(data), err => err != null ? reject(err) : fulfill());
            } else {
                reject('Actual session connection not found');
            }
        });
    }

    async enableTerminal(terminalSize: { cols: number, rows: number }) {
        if (this.term) {
            this.term.resize(terminalSize.cols, terminalSize.rows);
        } else {
            const term = this.term = spawn(
                IS_LOCAL ? 'ssh.exe' : 'tmux',
                IS_LOCAL ? ['-o', 'UserKnownHostsFile=' + DEV_NULL, '-o', 'StrictHostKeyChecking=no', '-i', PEM, 'root@' + REMOTE_HOSTNAME, '-t', 'tmux ' + tmuxArgs.join(' ')] : tmuxArgs,
                {
                    name: 'xterm-color',
                    cwd: process.env.HOME,
                    env: process.env,
                    ...terminalSize
                }
            );;
            await new Promise<void>((fullfill, reject) => this.socket.send(String.fromCharCode(27) + 'c', err => err ? reject(err) : fullfill()));
            term.onData(data => this.socket.send(data, dataSendCb));
            term.onExit(() => this.dispose());
        }
    }

    dispose() {
        if (this.term) {
            this.term.kill();
            this.term = undefined;
        }
        const c = SESSIONS.get(this.browserId);
        if (c?.get(this.tabId) === this) {
            c.delete(this.tabId);
        }
        if (
            this.socket.readyState != ServerWebSoket.CLOSING &&
            this.socket.readyState != ServerWebSoket.CLOSED
        ) {
            this.socket.close();
        }
    }

    ping(timeout: number) {
        if (Date.now() - this.lastActive >= timeout && this.socket.readyState === ServerWebSoket.OPEN) {
            this.socket.ping();
        }
    }
}