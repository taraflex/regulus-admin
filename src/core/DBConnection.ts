import natsort from 'alphanum-sort';
import fg from 'fast-glob';
import { promises as fs, statSync } from 'fs';
import timestamp from 'time-stamp';
import { Connection, createConnection, QueryRunner } from 'typeorm';
import { Provided } from 'typescript-ioc';

import { entities, repositories } from '@generated/entities';
import subscribers from '@generated/subscribers';
import { createAsync } from '@ioc/singleton-providers';
import { DEBUG } from '@utils/config';
import dataPath from '@utils/data-path';
import { emptyFile } from '@utils/is-empty-file';

export function createDBFilename() {
    return APP_NAME + '.' + timestamp('YYYY.MM.DD[HH.mm.ss]') + '.sqlite3';
}

async function generateMigrations(connection: Connection): Promise<Function[]> {
    const { upQueries, downQueries } = await connection.driver.createSchemaBuilder().log();
    if (upQueries.length > 0) {
        const migration = new Function(`return function _${Date.now()}() { }`)();
        //todo продумать, как определить миграции когда меняются только индексы
        //создаем миграцию если число DROP === CREATE и нет никаких других команд 
        const s = upQueries.reduce((s, q) => s + +q.query.startsWith('DROP INDEX '), 0);
        if (s !== upQueries.length / 2 || s !== upQueries.reduce((s, q) => s + +q.query.startsWith('CREATE INDEX '), 0)) {
            migration.prototype.up = async (queryRunner: QueryRunner) => {
                for (const { query } of upQueries) {
                    LOG_INFO(query);
                    await queryRunner.query(query);
                }
            };
            migration.prototype.down = async (queryRunner: QueryRunner) => {
                for (const { query } of downQueries) {
                    LOG_INFO(query);
                    await queryRunner.query(query);
                }
            };
            return [migration];
        }
    }
    return [];
}

async function getConnection(database: string, migrations?: Function[]) {
    const hasMigrations = Array.isArray(migrations) && migrations.length > 0;
    const connection = await createConnection({
        type: 'sqlite',
        database: database,
        synchronize: !hasMigrations && emptyFile(database),
        logging: DEBUG ? ["error", "warn", "schema", "migration"] : ["error", "schema", "migration"],
        entities: entities as Function[],
        subscribers: subscribers as Function[],
        migrationsRun: hasMigrations,
        migrations: migrations
    });
    Object.defineProperty(connection, 'filename', { value: database });
    return connection;
}

let sqliteFilename: string = null;

const provider = createAsync(async (): Promise<Connection> => {
    const alldb: string[] = natsort(await fg(APP_NAME + '.*].sqlite3', {
        cwd: dataPath(),
        absolute: true,
        extglob: false,
        braceExpansion: false,
        caseSensitiveMatch: false,
        baseNameMatch: true,
        globstar: false
    })).reverse();

    sqliteFilename = alldb.find(f => {
        const stat = statSync(f);
        return stat.size > 4096;
    }) || dataPath(createDBFilename());

    try {
        await Promise.all(alldb.map(f => f !== sqliteFilename && fs.unlink(f)));
    } catch (err) {
        LOG_ERROR(err);
    }

    let connection = await getConnection(sqliteFilename);

    const migrations = await generateMigrations(connection);
    if (migrations.length > 0) {
        await connection.close();
        connection = await getConnection(sqliteFilename, migrations);
    }

    repositories.forEach(r => r.provider.init(connection));

    for (const e of entities) {
        await e.asyncProvider?.init(connection);
    }

    return connection;
});

export const dbInit = provider.init;

@Provided(provider)
export abstract class DBConnection extends Connection {
    readonly filename: string;
}