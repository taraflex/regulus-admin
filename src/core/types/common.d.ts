declare const __non_webpack_require__: typeof require;
declare const __webpack_require__: (typeof require) & { p: string };
declare const __LINE__: number;
declare const BROWSER: boolean;
declare const APP_NAME: string;
declare const APP_TITLE: string;

declare const LOG_ERROR: (...args: any[]) => void;
declare const LOG_WARN: (...args: any[]) => void;
declare const LOG_INFO: (...args: any[]) => void;

declare const enum RowState {
    NONE = 0,
    EDIT = 1,
    SAVE = 2,
    REMOVE = 3
}

declare const enum SortType {
    NONE = 0,
    ASC = 1,
    DESC = 2
}

declare const enum RTTIItemState {
    NORMAL = 0,
    SEND_ALWAYS = 1 << 1,
    // 1 << 3,
    PRIMARY = 1 << 4 | SEND_ALWAYS,
    READONLY = 1 << 5 | SEND_ALWAYS,
    HIDDEN = 1 << 6 | READONLY,
}

declare const enum FlashLevel {
    ERROR = 0,
    WARNING = 1,
    INFO = 2,
}

declare type FlashData = Readonly<{
    messages?: string[],
    warnings?: string[],
    errors?: string[]
}>;