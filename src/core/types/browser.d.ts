declare module '@generated/frontend' {
    const _: any[];
    export default _;
}

declare module '*.vue' {
    const _: Function;
    export default _;
}

declare const ModelViews: any[];
declare const Paths: {
    health: string,
    resetemail: string,
    root: string,
    backup: string,
    restore: string,
    restart: string,
    logout: string,
    login: string,
    resetpass: string,
    [name: string]: string | Function
};
declare const UserInfo: { email: string, [name: string]: any };
declare const Flash: FlashData;
declare const MONACO_WORKER_URLS: {
    "editor.worker": { "js": string },
    "typescript.worker": { "js": string }
};
declare const LIB_URLS: Record<string, { js: string, css?: string }>;