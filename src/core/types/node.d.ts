declare const v8debug: any;

declare module '*.pug' {
    export default function (locals?: Record<string, any>): string;
}

declare module '*.mustache' {
    export default function (locals?: Record<string, any>): string;
}

declare module '@generated/entities' {
    export const entities: import('@crud').EntityClass[];
    export const repositories: import('@crud').RepositoryClass[];
}

declare module '@generated/subscribers' {
    const subscribers: import('typeorm').EntitySubscriberInterface[];
    export default subscribers;
}